# POP3 Proxy
[![wercker status](https://app.wercker.com/status/de6704c34dc53a3e39805b9372b010e7/s/master "wercker status")](https://app.wercker.com/project/bykey/de6704c34dc53a3e39805b9372b010e7)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/4b713106cf8941c6bc1da3ebc75a19fc)](https://www.codacy.com/app/jporsay/pdc2016q1?utm_source=iLNaNo@bitbucket.org&amp;utm_medium=referral&amp;utm_content=iLNaNo/pdc2016q1&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/4b713106cf8941c6bc1da3ebc75a19fc)](https://www.codacy.com/app/jporsay/pdc2016q1?utm_source=iLNaNo@bitbucket.org&amp;utm_medium=referral&amp;utm_content=iLNaNo/pdc2016q1&amp;utm_campaign=Badge_Coverage)

## Project setup
### Intellij Idea
From the root of the repository run:
```
./gradlew idea
```
And open `cafe.ipr`

To run from within intellij, open Main.java, right click in main method and click run/debug
### Eclipse
From the root of the repository run:
```
./gradlew eclipse
```
## Running from console
* Running proxy
```
./gradlew -q run
```
* Running commander sandbox
```
./gradlew -q runCommander
```
## Packaging for distribution
```
./gradlew assembleDist
```
## Testing
```
./gradlew test
```

## MUA Setup

Proxy POP3 port: 3232

## Controlling proxy

Run commands from the command line:
```
echo "CGET" | nc -c proxy_address 3131
```
