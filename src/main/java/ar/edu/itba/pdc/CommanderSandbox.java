package ar.edu.itba.pdc;

import ar.edu.itba.pdc.admin.CommandFactory;
import ar.edu.itba.pdc.admin.command.Command;
import ar.edu.itba.pdc.config.Config;
import ar.edu.itba.pdc.config.ConfigDao;
import ar.edu.itba.pdc.config.PropertyConfigDao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class CommanderSandbox {
  /**
   * Main method for the commander sandbox.
   *
   * @param args won't be used
   * @throws IOException if there's a problem reading STDIN
   */
  public static void main(String[] args) throws IOException {
    ConfigDao dao = new PropertyConfigDao(Config.get().getPropertyConfigPath());
    Config.get().setDao(dao);
    boolean run = true;
    InputStreamReader reader = new InputStreamReader(System.in, StandardCharsets.UTF_8);
    BufferedReader br = new BufferedReader(reader);
    while (run) {
      System.out.println("-------- Awaiting command --------");
      String line = br.readLine();
      if (line == null || line.equalsIgnoreCase("exit")) {
        run = false;
        br.close();
      } else {
        Command command = (new CommandFactory()).fromLine(line);
        System.out.println("< " + command.execute().getMessage());
      }
    }
  }
}
