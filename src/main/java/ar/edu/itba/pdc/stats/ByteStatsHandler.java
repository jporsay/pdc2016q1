package ar.edu.itba.pdc.stats;

import ar.edu.itba.pdc.admin.Stats;
import ar.edu.itba.pdc.net.async.Promise;
import ar.edu.itba.pdc.net.channel.HandlerContext;
import ar.edu.itba.pdc.net.channel.handler.OutputHandler;

import java.nio.ByteBuffer;

public class ByteStatsHandler implements OutputHandler {

  @Override
  public void write(HandlerContext ctx, Object msg, Promise promise) throws Exception {
    Stats.bytesProcessed.addAndGet(((ByteBuffer) msg).position());
    ctx.write(msg);
  }
}
