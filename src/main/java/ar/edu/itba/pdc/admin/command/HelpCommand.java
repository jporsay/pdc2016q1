package ar.edu.itba.pdc.admin.command;

import ar.edu.itba.pdc.admin.CommandFactory;
import ar.edu.itba.pdc.admin.response.CommandResponse;
import ar.edu.itba.pdc.admin.response.OkResponse;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class HelpCommand implements Command {

  public static final String COMMAND = "HELP";

  /*
   * Each help entry is in the form of:
   * COMMAND params
   *   description
   * COMMAND params
   *   description
   */

  @Override
  public CommandResponse execute() {
    List<String> commandHelpList = new LinkedList<>();
    CommandFactory.COMMANDS.forEach((key, value) -> {
      Command command = value.apply(null);
      commandHelpList.add(this.buildHelp(command.getUsage(), command.getDescription()));
    });
    Optional<String> help = commandHelpList.stream().reduce((me, other) -> me + "\n" + other);
    return new OkResponse("\n" + help.get());
  }

  @Override
  public String getUsage() {
    return COMMAND;
  }

  @Override
  public String getDescription() {
    return "Show this help";
  }

  private String buildHelp(String usage, String description) {
    return usage + "\n  " + description;
  }
}
