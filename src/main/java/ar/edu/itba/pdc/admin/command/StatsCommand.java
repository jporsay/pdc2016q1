package ar.edu.itba.pdc.admin.command;

import ar.edu.itba.pdc.admin.Stats;
import ar.edu.itba.pdc.admin.response.CommandResponse;
import ar.edu.itba.pdc.admin.response.OkResponse;

public class StatsCommand implements Command {

  public static final String COMMAND = "STAT";

  @Override
  public CommandResponse execute() {
    StringBuilder builder = new StringBuilder();
    builder.append(this.statLine("bytes_processed", Stats.bytesProcessed.toString()));
    builder.append(this.statLine("accepted_connections", Stats.acceptedConnections.toString()));
    builder.append(this.statLine("images_rotated", Stats.imagesRotated.toString()));
    builder.append(this.statLine("leetified_subjects", Stats.leetifiedSubjects.toString()));
    builder.append(this.statLine("emails_processed", Stats.emailsProcessed.toString(), true));
    return new OkResponse("\n" + builder.toString());
  }

  @Override
  public String getUsage() {
    return COMMAND;
  }

  @Override
  public String getDescription() {
    return "Display proxy statistics";
  }

  private String statLine(String key, String value) {
    return this.statLine(key, value, false);
  }

  private String statLine(String key, String value, boolean last) {
    if (last) {
      return "STAT" + " " + key + " " + value;
    }
    return "STAT" + " " + key + " " + value + "\n";
  }
}
