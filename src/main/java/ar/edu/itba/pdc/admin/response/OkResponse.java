package ar.edu.itba.pdc.admin.response;

public class OkResponse extends CommandResponse {

  public OkResponse(String message) {
    super(false, message);
  }

  public OkResponse() {
    super(false, null);
  }
}
