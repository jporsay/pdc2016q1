package ar.edu.itba.pdc.admin.response;

public class MisuseErrorResponse extends ErrorResponse {

  public MisuseErrorResponse(String usage) {
    super("usage: " + usage);
  }
}
