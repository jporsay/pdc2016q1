package ar.edu.itba.pdc.admin.command;

import ar.edu.itba.pdc.admin.response.CommandResponse;
import ar.edu.itba.pdc.admin.response.MisuseErrorResponse;
import ar.edu.itba.pdc.admin.response.OkResponse;
import ar.edu.itba.pdc.config.Config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;

public class DefaultServerSetCommand extends PayloadedCommand implements Command {

  public static final String COMMAND = "DSET";
  private static final Logger logger = LoggerFactory.getLogger(DefaultServerSetCommand.class);

  public DefaultServerSetCommand(String payload) {
    super(payload);
  }

  @Override
  public CommandResponse execute() {
    InetSocketAddress defaultServer = this.getDefaultServer();
    if (defaultServer == null) {
      return new MisuseErrorResponse(this.getUsage());
    }
    Config.get().setDefaultServer(defaultServer);
    return new OkResponse();
  }

  private InetSocketAddress getDefaultServer() {
    if (this.getPayload().isEmpty()) {
      return null;
    }
    String[] serverAndPort = this.getPayload().split(":");
    if (serverAndPort.length != 2) {
      logger.info("Wrong server format: {}", this.getPayload());
      return null;
    }
    return new InetSocketAddress(serverAndPort[0], Integer.parseInt(serverAndPort[1]));
  }

  @Override
  public String getUsage() {
    return COMMAND + " serverAddress:port";
  }

  @Override
  public String getDescription() {
    return "Set default POP3 server";
  }
}
