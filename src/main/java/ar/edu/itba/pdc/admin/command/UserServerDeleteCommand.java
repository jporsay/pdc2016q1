package ar.edu.itba.pdc.admin.command;

import ar.edu.itba.pdc.admin.response.CommandResponse;
import ar.edu.itba.pdc.admin.response.MisuseErrorResponse;
import ar.edu.itba.pdc.admin.response.OkResponse;
import ar.edu.itba.pdc.config.Config;

public class UserServerDeleteCommand extends PayloadedCommand implements Command {

  public static final String COMMAND = "UDEL";

  public UserServerDeleteCommand(String payload) {
    super(payload);
  }

  @Override
  public CommandResponse execute() {
    String user = this.getUserFromPayload();
    if (user == null) {
      return new MisuseErrorResponse(this.getUsage());
    }
    Config.get().removeUserServerException(user);
    return new OkResponse();
  }

  private String getUserFromPayload() {
    if (this.getPayload().isEmpty()) {
      return null;
    }
    String payload = this.getPayload().trim();
    if (payload.split(" ").length > 1) {
      return null;
    }
    return payload;
  }

  @Override
  public String getUsage() {
    return COMMAND + " username";
  }

  @Override
  public String getDescription() {
    return "Delete server override for given user";
  }
}
