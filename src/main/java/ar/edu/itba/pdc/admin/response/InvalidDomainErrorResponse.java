package ar.edu.itba.pdc.admin.response;

public class InvalidDomainErrorResponse extends ErrorResponse {

  public InvalidDomainErrorResponse(String domain) {
    super("invalid or non-existent domain: " + domain);
  }
}
