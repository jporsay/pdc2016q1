package ar.edu.itba.pdc.admin;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class Stats {

  public static final AtomicInteger acceptedConnections = new AtomicInteger(0);
  public static final AtomicLong bytesProcessed = new AtomicLong(0);
  public static final AtomicInteger imagesRotated = new AtomicInteger(0);
  public static final AtomicInteger leetifiedSubjects = new AtomicInteger(0);
  public static final AtomicInteger emailsProcessed = new AtomicInteger(0);

  private Stats() {
  }
}
