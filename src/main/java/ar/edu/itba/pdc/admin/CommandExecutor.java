package ar.edu.itba.pdc.admin;

import ar.edu.itba.pdc.admin.command.Command;
import ar.edu.itba.pdc.net.codec.Encoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommandExecutor extends Encoder<Command, String> {
  private static final Logger logger = LoggerFactory.getLogger(CommandExecutor.class);

  @Override
  protected boolean shouldEncode(Object msg) {
    return msg instanceof Command;
  }

  @Override
  protected String encode(Command msg) {
    logger.info("executed command {}.", msg.getClass());
    return msg.execute().toString();
  }
}
