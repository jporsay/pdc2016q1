package ar.edu.itba.pdc.admin.command;

class PayloadedCommand {
  private final String payload;

  PayloadedCommand(String payload) {
    this.payload = payload == null ? "" : payload.trim();
  }

  public String getPayload() {
    return this.payload;
  }
}
