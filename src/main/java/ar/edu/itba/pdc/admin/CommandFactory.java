package ar.edu.itba.pdc.admin;

import ar.edu.itba.pdc.admin.command.Command;
import ar.edu.itba.pdc.admin.command.ConfigGetCommand;
import ar.edu.itba.pdc.admin.command.DefaultServerSetCommand;
import ar.edu.itba.pdc.admin.command.ErrorCommand;
import ar.edu.itba.pdc.admin.command.HelpCommand;
import ar.edu.itba.pdc.admin.command.ImageFlipSetCommand;
import ar.edu.itba.pdc.admin.command.LeetModeSetCommand;
import ar.edu.itba.pdc.admin.command.StatsCommand;
import ar.edu.itba.pdc.admin.command.UserServerDeleteCommand;
import ar.edu.itba.pdc.admin.command.UserServerSetCommand;
import ar.edu.itba.pdc.net.codec.Decoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.function.Function;

public class CommandFactory extends Decoder<String, Command> {
  private static final Logger logger = LoggerFactory.getLogger(CommandFactory.class);
  public static final Map<String, Function<String, Command>> COMMANDS;

  static {
    Map<String, Function<String, Command>> commandMap = new HashMap<>();
    // Payloaded commands
    commandMap.put(DefaultServerSetCommand.COMMAND, DefaultServerSetCommand::new);
    commandMap.put(UserServerSetCommand.COMMAND, UserServerSetCommand::new);
    commandMap.put(UserServerDeleteCommand.COMMAND, UserServerDeleteCommand::new);
    commandMap.put(LeetModeSetCommand.COMMAND, LeetModeSetCommand::new);
    commandMap.put(ImageFlipSetCommand.COMMAND, ImageFlipSetCommand::new);
    // Empty commands
    commandMap.put(ConfigGetCommand.COMMAND, (payload) -> new ConfigGetCommand());
    commandMap.put(StatsCommand.COMMAND, (payload) -> new StatsCommand());
    commandMap.put(HelpCommand.COMMAND, (payload) -> new HelpCommand());
    COMMANDS = Collections.unmodifiableMap(commandMap);
  }

  /**
   * Parse a command from a command line.
   *
   * @param line input line
   * @return parsed command
   */
  public Command fromLine(String line) {
    String trimmedLine = line.trim();
    int lineLength = trimmedLine.length();
    if (lineLength < 4) {
      return new ErrorCommand();
    }
    if (lineLength > 4 && !trimmedLine.substring(4, 5).equals(" ")) {
      return new ErrorCommand();
    }
    String commandName = trimmedLine.substring(0, 4).toUpperCase();
    String payload = trimmedLine.substring(4);
    if (COMMANDS.containsKey(commandName)) {
      Function<String, Command> constructor = COMMANDS.get(commandName);
      return constructor.apply(payload);
    }
    return new ErrorCommand();
  }

  @Override
  protected boolean shouldDecode(Object msg) {
    return msg instanceof String;
  }

  @Override
  protected void decode(String msg, Queue<Command> out) {
    Command command = this.fromLine(msg);
    logger.info("decoded command {} type of {}.", msg, command.getClass());
    out.add(command);
  }
}
