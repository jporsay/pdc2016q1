package ar.edu.itba.pdc.admin.command;

import ar.edu.itba.pdc.admin.response.CommandResponse;
import ar.edu.itba.pdc.admin.response.OkResponse;
import ar.edu.itba.pdc.config.Config;

import java.net.InetSocketAddress;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class ConfigGetCommand implements Command {

  public static final String COMMAND = "CGET";

  @Override
  public CommandResponse execute() {
    Config config = Config.get();
    List<String> configs = new LinkedList<>();
    configs.add(this.toLine("defaultServer",
        this.encodeInetSocketAddress(config.getDefaultServerEndpoint()))
    );
    configs.add(this.toLine("imageFlip", String.valueOf(config.isImageFlipEnabled())));
    configs.add(this.toLine("leetMode", String.valueOf(config.isLeetEnabled())));
    configs.add(this.toLine("serverExceptions", this.getServerExceptionString(config)));
    return new OkResponse("\n" + configs.stream().reduce((first, second) ->
        first + "\n" + second).get()
    );
  }

  private String getServerExceptionString(Config config) {
    Optional<String> users = config.getUserServerExceptions().entrySet()
        .stream()
        .map(entry -> entry.getKey() + "@" + this.encodeInetSocketAddress(entry.getValue()))
        .reduce((entry1, entry2) -> entry1 + "," + entry2);
    return users.orElse("none");
  }

  @Override
  public String getUsage() {
    return COMMAND;
  }

  @Override
  public String getDescription() {
    return "Show current configuration parameters";
  }

  private String toLine(String name, String value) {
    return String.format("CONF %s %s", name, value);
  }

  private String encodeInetSocketAddress(InetSocketAddress address) {
    return address.getHostName() + ":" + address.getPort();
  }
}
