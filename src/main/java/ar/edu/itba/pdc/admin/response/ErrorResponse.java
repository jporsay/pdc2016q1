package ar.edu.itba.pdc.admin.response;

public class ErrorResponse extends CommandResponse {

  public ErrorResponse(String message) {
    super(true, message);
  }


}
