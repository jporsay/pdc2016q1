package ar.edu.itba.pdc.admin.command;

import ar.edu.itba.pdc.admin.response.CommandResponse;
import ar.edu.itba.pdc.admin.response.OkResponse;
import ar.edu.itba.pdc.config.Config;

public class LeetModeSetCommand extends BooleanCommand {

  public static final String COMMAND = "LSET";

  public LeetModeSetCommand(String payload) {
    super(payload);
  }

  @Override
  public CommandResponse doExecute(boolean leetEnabled) {
    Config.get().setLeetEnabled(leetEnabled);
    return new OkResponse();
  }

  @Override
  public String getCommandName() {
    return COMMAND;
  }

  @Override
  public String getDescription() {
    return "Enable or disable l33t mail title mode";
  }
}
