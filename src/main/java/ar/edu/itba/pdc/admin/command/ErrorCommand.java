package ar.edu.itba.pdc.admin.command;

import ar.edu.itba.pdc.admin.response.CommandResponse;
import ar.edu.itba.pdc.admin.response.ErrorResponse;

public class ErrorCommand implements Command {

  @Override
  public CommandResponse execute() {
    return new ErrorResponse("unknown command");
  }

  @Override
  public String getUsage() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
