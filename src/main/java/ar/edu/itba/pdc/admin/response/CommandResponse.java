package ar.edu.itba.pdc.admin.response;

public class CommandResponse {

  private final boolean error;
  private final String message;

  public CommandResponse(boolean error, String message) {
    this.error = error;
    this.message = this.buildMessage(message);
  }

  private String buildMessage(String message) {
    boolean nullMessage = message == null;
    return (this.error ? "-ERR" : "+OK").concat(nullMessage ? "" : " " + message);
  }

  public String getMessage() {
    return this.message;
  }

  public boolean isError() {
    return this.error;
  }

  @Override
  public String toString() {
    return this.message;
  }
}
