package ar.edu.itba.pdc.admin;

import ar.edu.itba.pdc.net.ChannelInitializer;
import ar.edu.itba.pdc.net.channel.Channel;
import ar.edu.itba.pdc.net.channel.handler.EchoHandler;
import ar.edu.itba.pdc.net.codec.ByteBufferToLineDecoder;
import ar.edu.itba.pdc.net.codec.LineToByteBufferEncoder;

public class AdminInitializer extends ChannelInitializer {

  public AdminInitializer() {
    super("ADMIN");
  }

  @Override
  protected void initChannel(Channel channel) throws Exception {
    Stats.acceptedConnections.addAndGet(1);
    channel.getPipeline().addLast("decodeLine", new ByteBufferToLineDecoder());
    channel.getPipeline().addLast("decodeCommand", new CommandFactory());
    channel.getPipeline().addLast("encodeLine", new LineToByteBufferEncoder());
    channel.getPipeline().addLast("executeCommand", new CommandExecutor());
    channel.getPipeline().addLast("echo", new EchoHandler());
    channel.read();
  }
}
