package ar.edu.itba.pdc.admin.command;

import ar.edu.itba.pdc.admin.response.CommandResponse;
import ar.edu.itba.pdc.admin.response.MisuseErrorResponse;

abstract class BooleanCommand extends PayloadedCommand implements Command {
  BooleanCommand(String payload) {
    super(payload);
  }

  @Override
  public CommandResponse execute() {
    return this.processPayload();
  }

  protected abstract CommandResponse doExecute(boolean payload);

  private CommandResponse processPayload() {
    if (this.getPayload().equalsIgnoreCase("1")) {
      return this.doExecute(true);
    }
    if (this.getPayload().equalsIgnoreCase("0")) {
      return this.doExecute(false);
    }
    return new MisuseErrorResponse(this.getUsage());
  }

  @Override
  public final String getUsage() {
    return this.getCommandName()  + " 1 or 0";
  }

  public abstract String getCommandName();
}
