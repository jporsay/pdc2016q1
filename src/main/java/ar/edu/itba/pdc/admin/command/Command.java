package ar.edu.itba.pdc.admin.command;

import ar.edu.itba.pdc.admin.response.CommandResponse;

public interface Command {
  CommandResponse execute();

  String getUsage();

  String getDescription();
}
