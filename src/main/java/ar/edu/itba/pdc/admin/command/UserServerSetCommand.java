package ar.edu.itba.pdc.admin.command;

import ar.edu.itba.pdc.admin.response.CommandResponse;
import ar.edu.itba.pdc.admin.response.MisuseErrorResponse;
import ar.edu.itba.pdc.admin.response.OkResponse;
import ar.edu.itba.pdc.config.Config;

import java.net.InetSocketAddress;

public class UserServerSetCommand extends PayloadedCommand implements Command {

  public static final String COMMAND = "USET";
  private String user = "";
  private InetSocketAddress domain;

  public UserServerSetCommand(String payload) {
    super(payload);
  }

  @Override
  public CommandResponse execute() {
    this.processPayload();
    if (this.user.isEmpty() || this.domain == null) {
      return new MisuseErrorResponse(this.getUsage());
    }
    Config.get().setUserServerException(this.user, this.domain);
    return new OkResponse();
  }

  private void processPayload() {
    String[] parts = this.getPayload().replaceAll(" +", " ").split(" ");
    if (parts.length != 2) {
      return;
    }
    this.user = parts[0];
    String[] serverAndPort = parts[1].split(":");
    if (serverAndPort.length != 2) {
      return;
    }
    this.domain = new InetSocketAddress(serverAndPort[0], Integer.parseInt(serverAndPort[1]));
  }

  @Override
  public String getUsage() {
    return COMMAND + " username serverAddress:port";
  }

  @Override
  public String getDescription() {
    return "Add server override for given user";
  }
}
