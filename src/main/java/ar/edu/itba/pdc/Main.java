package ar.edu.itba.pdc;

import ar.edu.itba.pdc.admin.AdminInitializer;
import ar.edu.itba.pdc.config.Config;
import ar.edu.itba.pdc.config.InMemoryConfigDao;
import ar.edu.itba.pdc.mime.MimeParserInitializer;
import ar.edu.itba.pdc.net.ChannelInitializer;
import ar.edu.itba.pdc.net.Nexus;
import ar.edu.itba.pdc.net.channel.Channel;
import ar.edu.itba.pdc.net.channel.handler.EchoHandler;
import ar.edu.itba.pdc.proxy.ProxyInitializer;
import ar.edu.itba.pdc.stats.ByteStatsHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.InetSocketAddress;

public class Main {

  private static final Logger logger = LoggerFactory.getLogger(Main.class);

  /**
   * Main program for the proxy.
   *
   * @param args command line arguments. (not-used)
   */
  public static void main(String[] args) {
    Config config = Config.get();
    config.setDao(new InMemoryConfigDao());
    config.load();
    try {
      long timeoutMs = 5000;
      Nexus nexus = new Nexus(timeoutMs);
      nexus.bind(config.getAdminEndpoint(), new AdminInitializer());
      nexus.bind(config.getProxyEndpoint(), new ProxyInitializer());
      nexus.bind(
          new InetSocketAddress(InetAddress.getLoopbackAddress(), 3434),
          new MimeParserInitializer()
      );

      nexus.bind(
          new InetSocketAddress(InetAddress.getLoopbackAddress(), 3030),
          new ChannelInitializer("ECHO_SERVICE") {
            @Override
            protected void initChannel(Channel channel) throws Exception {
              channel.getPipeline().addLast("BYTE_STATS", new ByteStatsHandler());
              channel.getPipeline().addLast("ECHO", new EchoHandler());
              channel.read();
            }
          }
      );
      new Thread(nexus, "NEXUS").start();
    } catch (Exception ex) {
      logger.error("Unhandled Exception", ex);
    }
  }
}
