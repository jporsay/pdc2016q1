package ar.edu.itba.pdc.transform;

public class LeetTransform {

  /**
   * Transform given text to l33t text.
   *
   * @return L33T converted text
   */
  public String transform(String text) {
    int length = text.length();
    StringBuilder stringBuilder = new StringBuilder(length);
    for (int i = 0; i < length; ++i) {
      stringBuilder.append(this.transformChar(text.charAt(i)));
    }
    return stringBuilder.toString();
  }

  /**
   * Transform given char to l33t.
   *
   * @return L33T converted char
   */
  public char transform(char character) {
    return this.transformChar(character);
  }

  private char transformChar(char current) {
    if ('a' == current) { // || 'A' == current) {
      return '4';
    }
    if ('e' == current) { // || 'E' == current) {
      return '3';
    }
    if ('i' == current) { // || 'I' == current) {
      return '1';
    }
    if ('o' == current) { // || 'O' == current) {
      return '0';
    }
    if ('c' == current) { // || 'C' == current) {
      return '<';
    }
    return current;
  }
}
