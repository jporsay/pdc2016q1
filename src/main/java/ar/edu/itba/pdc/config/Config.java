package ar.edu.itba.pdc.config;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class Config {
  private static final Config instance = new Config();
  private final Map<String, InetSocketAddress> userServerExceptionMap;
  private boolean leetEnabled = true;
  private boolean imageFlipEnabled = true;
  private ConfigDao dao;
  private InetSocketAddress defaultServer;
  private InetSocketAddress adminEndpoint;
  private InetSocketAddress proxyEndpoint;

  private Config() {
    this.userServerExceptionMap = new HashMap<>();
    this.defaultServer = new InetSocketAddress(InetAddress.getLoopbackAddress(), 3110);
    this.adminEndpoint = new InetSocketAddress(InetAddress.getLoopbackAddress(), 3131);
    this.proxyEndpoint = new InetSocketAddress(3232);
  }

  public static Config get() {
    return instance;
  }

  public InetSocketAddress getAdminEndpoint() {
    return this.adminEndpoint;
  }

  public InetSocketAddress getProxyEndpoint() {
    return this.proxyEndpoint;
  }

  public void load() {
    this.dao.load();
  }

  public void setDao(ConfigDao dao) {
    this.dao = dao;
  }

  public InetSocketAddress getDefaultServerEndpoint() {
    return this.defaultServer;
  }

  public void setDefaultServer(InetSocketAddress defaultServer) {
    this.defaultServer = defaultServer;
    this.persist();
  }

  public boolean shouldProcessResponses() {
    return this.isLeetEnabled() || this.isImageFlipEnabled();
  }

  public boolean isLeetEnabled() {
    return this.leetEnabled;
  }

  public void setLeetEnabled(boolean leetEnabled) {
    this.leetEnabled = leetEnabled;
    this.persist();
  }

  public boolean isImageFlipEnabled() {
    return this.imageFlipEnabled;
  }

  public void setImageFlipEnabled(boolean imageFlipEnabled) {
    this.imageFlipEnabled = imageFlipEnabled;
    this.persist();
  }

  public void clearUserServerExceptions() {
    this.userServerExceptionMap.clear();
    this.persist();
  }

  public Map<String, InetSocketAddress> getUserServerExceptions() {
    return this.userServerExceptionMap;
  }

  public InetSocketAddress getUserServerException(String user) {
    return this.userServerExceptionMap.getOrDefault(user, this.getDefaultServerEndpoint());
  }

  public void setUserServerExceptionMap(Map<String, InetSocketAddress> map) {
    map.forEach(this::setUserServerException);
  }

  public void setUserServerException(String user, InetSocketAddress server) {
    this.userServerExceptionMap.put(user, server);
    this.persist();
  }

  public void removeUserServerException(String user) {
    this.userServerExceptionMap.remove(user);
    this.persist();
  }

  private void persist() {
    this.dao.write(this);
  }

  /**
   * Get the path for the persisted config.
   *
   * @return Path for property
   */
  public Path getPropertyConfigPath() {
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    String path = classLoader.getResource("config.properties").getPath();
    String osAppropriatePath = System.getProperty("os.name").contains("indow")
        ? path.substring(1) : path;
    return Paths.get(osAppropriatePath);
  }
}
