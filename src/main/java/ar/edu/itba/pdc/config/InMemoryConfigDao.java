package ar.edu.itba.pdc.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InMemoryConfigDao implements ConfigDao {

  private static final Logger logger = LoggerFactory.getLogger(InMemoryConfigDao.class);

  @Override
  public void load() {
    logger.info("Loaded config");
  }

  @Override
  public void write(Config config) {
    logger.info("Wrote config");
  }
}
