package ar.edu.itba.pdc.config;

public interface ConfigDao {
  void load();

  void write(Config config);
}
