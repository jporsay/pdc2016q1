package ar.edu.itba.pdc.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Properties;

public class PropertyConfigDao implements ConfigDao {

  private static final Logger logger = LoggerFactory.getLogger(PropertyConfigDao.class);
  private final Path configPath;

  public PropertyConfigDao(Path configPath) {
    this.configPath = configPath;
  }

  /**
   * Load global proxy configuration from property file.
   */
  public void load() {
    try {
      Properties properties = new Properties();
      properties.load(Files.newInputStream(this.configPath, StandardOpenOption.READ));
      (new PropertyConfig(properties)).populateConfig(Config.get());
    } catch (IOException ex) {
      logger.error(ex.getMessage(), ex);
    }
  }

  /**
   * Persist configuration to property file.
   *
   * @param config Config to persist
   */
  public void write(Config config) {
    try {
      (new PropertyConfig())
          .toProperties(config)
          .store(Files.newOutputStream(this.configPath, StandardOpenOption.WRITE), "-NONE-");
    } catch (IOException ex) {
      logger.error(ex.getMessage(), ex);
    }
  }
}
