package ar.edu.itba.pdc.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.Optional;
import java.util.Properties;

public class PropertyConfig {

  private static final Logger logger = LoggerFactory.getLogger(PropertyConfig.class);
  private static final String LEET_KEY = "leet";
  private static final String FLIP_KEY = "image_flip";
  private static final String DEFAULT_SERVER_KEY = "default_server";
  private static final String USER_SERVER_EXCEPTION_KEY = "user_server_exception";
  private Properties properties;

  public PropertyConfig() {
    this(new Properties());
  }

  public PropertyConfig(Properties prop) {
    this.properties = prop;
  }

  /**
   * Setup a Config from a loaded Properties file.
   *
   * @param config config to setup
   */
  public void populateConfig(Config config) {
    String trueValue = "true";
    config.setLeetEnabled(trueValue.equals(this.properties.getOrDefault(LEET_KEY, trueValue)));
    config.setImageFlipEnabled(trueValue.equals(this.properties.getOrDefault(FLIP_KEY, trueValue)));
    String defaultServerAndPort = this.properties.getOrDefault(DEFAULT_SERVER_KEY, "").toString();
    String[] serverAndPort = defaultServerAndPort.split(":");
    if (serverAndPort.length != 2) {
      logger.warn("Invalid default_server. Loading default");
    } else {
      config.setDefaultServer(
          new InetSocketAddress(serverAndPort[0], Integer.parseInt(serverAndPort[1]))
      );
    }
    this.loadUserServerExceptions(this.properties, config);
  }

  /**
   * Create a Properties from a Config.
   *
   * @param conf information datasource
   * @return populated properties file
   */
  public Properties toProperties(Config conf) {
    this.properties.setProperty(LEET_KEY, String.valueOf(conf.isLeetEnabled()));
    this.properties.setProperty(FLIP_KEY, String.valueOf(conf.isImageFlipEnabled()));
    this.properties.setProperty(DEFAULT_SERVER_KEY,
        this.encodeInetSocketAddress(conf.getDefaultServerEndpoint())
    );
    this.properties.setProperty(USER_SERVER_EXCEPTION_KEY, this.encodeUserServerExceptionMap(conf));
    return this.properties;
  }

  private String encodeUserServerExceptionMap(Config config) {
    Optional<String> users = config.getUserServerExceptions().entrySet()
        .stream()
        .map(entry ->
            entry.getKey() + "@" + entry.getValue().getHostName() + ":" + entry.getValue().getPort()
        )
        .reduce((entry1, entry2) -> entry1 + "," + entry2);
    return users.orElse("");
  }

  private void loadUserServerExceptions(Properties prop, Config config) {
    Config.get().clearUserServerExceptions();
    String rawExceptions = prop.getOrDefault(USER_SERVER_EXCEPTION_KEY, "").toString();
    Arrays.stream(rawExceptions.split(","))
        .filter(item -> !item.isEmpty())
        .forEach((item) -> {
          if (item.contains("@")) {
            String[] usernameAndServer = item.split("@");
            String[] serverAndPort = usernameAndServer[1].split(":");
            if (serverAndPort.length != 2) {
              logger.warn("Using default server for {}", usernameAndServer[0]);
            } else {
              config.setUserServerException(
                  usernameAndServer[0],
                  new InetSocketAddress(serverAndPort[0],
                      Integer.valueOf(serverAndPort[1])
                  )
              );
            }
          }
        })
    ;
  }

  private String encodeInetSocketAddress(InetSocketAddress address) {
    return address.getHostName() + ":" + address.getPort();
  }
}
