package ar.edu.itba.pdc.image;

import ar.edu.itba.pdc.net.codec.Encoder;
import javaxt.io.Image;

import org.apache.commons.codec.binary.Base64;

import java.io.IOException;

public class ImageDecoder extends Encoder<StringBuilder, Image> {


  @Override
  protected boolean shouldEncode(Object msg) {
    return msg instanceof StringBuilder;
  }

  @Override
  protected Image encode(StringBuilder msg) throws IOException {
    Base64 codec = new Base64();
    return new Image(codec.decode(msg.toString()));
  }
}
