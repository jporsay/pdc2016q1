package ar.edu.itba.pdc.image;

import ar.edu.itba.pdc.net.codec.Encoder;
import javaxt.io.Image;

import org.apache.commons.codec.binary.Base64;

import java.nio.ByteBuffer;

public class ImageEncoder extends Encoder<Image, ByteBuffer> {

  private final byte[] endLineBytes;
  private final int lineLength;

  /**
   * @param lineLength length to divide line after rotate.
   * @param endLineBytes \r\n.
   */

  public ImageEncoder(int lineLength, byte[] endLineBytes) {
    this.lineLength = lineLength;
    this.endLineBytes = endLineBytes.clone();
  }

  @Override
  protected boolean shouldEncode(Object msg) {
    return msg instanceof Image;
  }

  @Override
  protected ByteBuffer encode(Image msg) {
    Base64 codec = new Base64(this.lineLength, this.endLineBytes);
    return ByteBuffer.wrap(codec.encode(msg.getByteArray())).compact();

  }

}
