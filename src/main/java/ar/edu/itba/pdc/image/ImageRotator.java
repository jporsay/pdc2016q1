package ar.edu.itba.pdc.image;

import ar.edu.itba.pdc.admin.Stats;
import ar.edu.itba.pdc.net.codec.Encoder;
import javaxt.io.Image;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ImageRotator extends Encoder<Image, Image> {
  private static final Logger logger = LoggerFactory.getLogger(ImageRotator.class);

  @Override
  protected boolean shouldEncode(Object msg) {
    return msg instanceof Image;
  }

  @Override
  protected Image encode(Image msg) {
    if (msg.getWidth() != 0) {
      msg.rotate(180);
      logger.info("rotating image {}", msg);
    }
    Stats.imagesRotated.addAndGet(1);
    return msg;
  }

}
