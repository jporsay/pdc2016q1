package ar.edu.itba.pdc.mime;

import ar.edu.itba.pdc.net.codec.Encoder;
import ar.edu.itba.pdc.transform.LeetTransform;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SubjectParserHandler extends Encoder<String, String> {

  @Override
  protected boolean shouldEncode(Object msg) {
    return msg instanceof String;
  }

  @Override
  protected String encode(String line) throws IOException {
    String[] groups = line.split("\\s+");
    StringBuilder lineTransformed = new StringBuilder();
    for (String partLine : groups) {
      if (partLine.toLowerCase().startsWith("subject:")) {
        lineTransformed.append(partLine);
      } else {
        lineTransformed.append(process(partLine));
      }
    }
    return lineTransformed.toString();
  }

  private String process(String line) throws IOException {
    Pattern pattern = Pattern.compile(
        "^(\\s*[sS][uU][bB][jJ][eE][cC][tT]:)?\\s*=\\?([\\w-\\d+]+)\\?(\\w+)\\?(.*)\\?=\\s*"
    );
    Matcher matcher = pattern.matcher(line);
    if (matcher.find()) {
      return this.processEncodedSubject(
          matcher.group(1), // header
          matcher.group(2), // charset
          matcher.group(3), // encoding
          matcher.group(4) // content
      );
    }
    return this.processPlainSubject(line);
  }

  private String processEncodedSubject(
      String header, String charset, String encoding,
      String content) throws UnsupportedEncodingException {
    String processedContent;
    if (encoding.equalsIgnoreCase("b")) { // Process base-64 line
      processedContent = this.transformBase64Subject(content, charset);
    } else {
      StringBuilder builder = new StringBuilder();
      int length = content.length();
      int counter = 0;
      for (int index = 0; index < length; ++index) {
        char current = content.charAt(index);
        if (current == '=') {
          counter = 2;
        }
        if (counter == 0) {
          LeetTransform transform = new LeetTransform();
          builder.append(transform.transform(current));
        } else {
          builder.append(current);
          --counter;
        }
      }
      processedContent = builder.toString();
    }
    return String.format(
        "%s=?%s?%s?%s?=", header != null ? header + " " : " ", charset, encoding, processedContent
    );

  }

  private String processPlainSubject(
      String line) throws UnsupportedEncodingException {
    Pattern nonEncodedPattern = Pattern.compile(
        "^(\\s*[sS][uU][bB][jJ][eE][cC][tT]:)?(\\s*)(.*)"
    );
    Matcher nonEncodedMatcher = nonEncodedPattern.matcher(line);
    if (!nonEncodedMatcher.find()) {
      return null; // Line is not a subject header or line
    }
    String header = nonEncodedMatcher.group(1);
    String padding = nonEncodedMatcher.group(2);
    LeetTransform transform = new LeetTransform();
    String leetContent = transform.transform(nonEncodedMatcher.group(3));
    return String.format(
        "%s%s%s", header != null ? header : " ", padding, leetContent
    );
  }

  private String transformBase64Subject(
      String subject, String charset
  ) throws UnsupportedEncodingException {
    byte[] decode = Base64.getDecoder().decode(subject);
    LeetTransform transform = new LeetTransform();
    String leetSubject = transform.transform(new String(decode, charset));
    decode = Base64.getEncoder().encode(leetSubject.getBytes(charset));
    return new String(decode, charset);
  }
}
