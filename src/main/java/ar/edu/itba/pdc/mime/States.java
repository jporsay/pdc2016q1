package ar.edu.itba.pdc.mime;


public enum States {

  MIME_HEADER, SUBJECT,
  WAIT_IMG, BUFFER_IMAGE, IN_SECTION, WAIT_FOR_BOUNDARY,
  WAIT_FOR_END_BOUNDARY, NO_PARSE


}
