package ar.edu.itba.pdc.mime;

import ar.edu.itba.pdc.admin.Stats;
import ar.edu.itba.pdc.config.Config;
import ar.edu.itba.pdc.image.ImageDecoder;
import ar.edu.itba.pdc.image.ImageEncoder;
import ar.edu.itba.pdc.image.ImageRotator;
import ar.edu.itba.pdc.net.async.Promise;
import ar.edu.itba.pdc.net.channel.ChannelPipeline;
import ar.edu.itba.pdc.net.channel.HandlerContext;
import ar.edu.itba.pdc.net.channel.handler.OutputHandler;
import javaxt.io.Image;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MimeAutomaton implements OutputHandler {
  private static final String TEXT_CHARSET = "UTF-8";
  private static final String SUBJECT_HEADER = "subject:";
  private static final String BOUNDARY_LIMIT = "--";
  private static final String END_LINE = "\r\n";
  private static final byte[] END_LINE_BYTES = END_LINE.getBytes(Charset.forName(TEXT_CHARSET));
  private static final int LINE_LENGTH = 76;
  private final boolean leetSubject;
  private final boolean rotateImages;
  private States state = States.MIME_HEADER;
  private StringBuilder imageContent;
  private boolean imageContentTypeFound = false;
  private boolean contentTransferEncodingFound = false;
  private Stack<String> boundaries = new Stack<>();
  private static final int IMG_SIZE_LIMIT = 50 * (int)Math.pow(2, 20);
  private int currentSize = 0;

  /**
   * Processes MIME messages rotating them and leetifying them.
   */
  public MimeAutomaton() {
    this.leetSubject = Config.get().isLeetEnabled();
    this.rotateImages = Config.get().isImageFlipEnabled();
    this.imageContent = new StringBuilder();
    if (!this.leetSubject && !this.rotateImages) {
      this.state = States.NO_PARSE;
    }
  }

  @Override
  public void write(HandlerContext ctx, Object msg, Promise promise)
                      throws IOException {
    if (this.state == States.NO_PARSE) {
      ctx.write(msg, promise);
      return;
    }
    String line = (String) msg;
    if (this.state == States.MIME_HEADER) {
      if (!this.leetSubject) {
        this.state = States.WAIT_FOR_BOUNDARY;
      } else if (line.toLowerCase().startsWith(SUBJECT_HEADER)) {
        Stats.leetifiedSubjects.addAndGet(1);
        this.state = States.SUBJECT;
        ctx.getPipeline().addBefore(
            "stateMachineParser",
            "subjectParser",
            new SubjectParserHandler());
      }
      ctx.write(line, promise);
      return;
    }
    if (this.state == States.SUBJECT
        && !line.startsWith("\t") && !line.startsWith(" ")) {
      ctx.getPipeline().remove("subjectParser");
      if (!this.rotateImages) {
        this.state = States.NO_PARSE;
        ctx.write(line, promise);
        return;
      }
      this.state = States.WAIT_FOR_BOUNDARY;
    }
    if (this.state == States.WAIT_FOR_BOUNDARY) {
      if (line.startsWith(BOUNDARY_LIMIT)) {
        if (this.boundaries.isEmpty() || !line.startsWith(this.boundaries.peek())) {
          this.boundaries.push(line);
        } else if (line.startsWith(this.boundaries.peek())
            && line.endsWith(BOUNDARY_LIMIT)) {
          this.boundaries.pop();
        }
        this.state = States.IN_SECTION;
      }
      ctx.write(line, promise);
      return;
    }
    if (this.state == States.IN_SECTION) {
      this.handleInSection(ctx, line, promise);
      return;
    }
    if (this.state == States.WAIT_IMG && line.isEmpty()) {
      this.state = States.BUFFER_IMAGE;
      ctx.write(line, promise);
      return;
    }
    if (this.state == States.BUFFER_IMAGE) {
      if (!line.startsWith(this.boundaries.peek())) {
        if (this.currentSize < IMG_SIZE_LIMIT) {
          this.imageContent.append(line);
          this.currentSize += line.length();
        } else {
          if (this.imageContent.length() != 0) {
            this.freeImageBuffer(ctx, promise);
          }
          ctx.write(line, promise);
        }
        return;
      } else {
        if (line.endsWith(BOUNDARY_LIMIT)) {
          this.state = States.WAIT_FOR_BOUNDARY;
          this.boundaries.pop();
        } else {
          this.state = States.IN_SECTION;
        }
        this.imageContentTypeFound = false;
        this.contentTransferEncodingFound = false;
        this.currentSize = 0;
        ctx.write(this.imageContent, promise);
        this.imageContent.setLength(0);
        this.removeImageHandlers(ctx);
        ctx.write(line);
      }
      return;
    }
    if (this.state == States.WAIT_FOR_END_BOUNDARY) {
      if (line.startsWith(this.boundaries.peek())
          && line.endsWith(BOUNDARY_LIMIT)) {
        this.state = States.WAIT_FOR_BOUNDARY;
        this.boundaries.pop();
        if (this.boundaries.empty()) {
          ctx.getPipeline().remove("mimeHandler");
        }
      } else if (line.startsWith(this.boundaries.peek())) {
        this.state = States.IN_SECTION;
      }
      ctx.write(line, promise);
      return;
    }
    ctx.write(line, promise);
  }

  private void handleInSection(HandlerContext ctx, String line, Promise promise) {
    if (!this.boundaries.empty() && line.startsWith(this.boundaries.peek())
        && line.endsWith(BOUNDARY_LIMIT)) {
      this.boundaries.pop();
      ctx.write(line, promise);
      return;
    }
    if (!this.boundaries.empty()) {
      this.searchForImageSection(line, ctx);
    }
    ctx.write(line, promise);
  }


  private void searchForImageSection(String line, HandlerContext ctx) {
    String lowerCaseLine = line.toLowerCase();
    // TODO: Check if image/* contains only letters and fix pattern if not
    Pattern contentTypePattern = Pattern.compile("^content-type:\\s*(\\w+)/(\\w+).*");
    Matcher contentTypeMatcher = contentTypePattern.matcher(lowerCaseLine);
    if (contentTypeMatcher.find()) {
      String contentType = contentTypeMatcher.group(1);
      String format = contentTypeMatcher.group(2);
      switch (contentType) {
        case "image":
          if (!this.isSupportedImageFormat(format)) {
            this.state = States.WAIT_FOR_END_BOUNDARY;
            return;
          }
          this.imageContentTypeFound = true;
          break;
        case "multipart":
          this.state = States.WAIT_FOR_BOUNDARY;
          this.contentTransferEncodingFound = false;
          break;
        default:
          this.state = States.WAIT_FOR_END_BOUNDARY;
          this.contentTransferEncodingFound = false;
          break;
      }
    }
    // TODO: Check if CTE can contain only letters and digits
    Pattern ctePattern = Pattern.compile("^content-transfer-encoding:\\s*([\\w\\d]+)");
    Matcher cteMatcher = ctePattern.matcher(lowerCaseLine);
    if (cteMatcher.find()) {
      this.contentTransferEncodingFound = true;
    }
    if (this.imageContentTypeFound && this.contentTransferEncodingFound) {
      this.state = States.WAIT_IMG;
      final String textCharset = "UTF-8";
      final String endLine = "\r\n";
      final byte[] endLineBytes = endLine.getBytes(Charset.forName(textCharset));
      final int lineLength = 76;
      ChannelPipeline pipeline = ctx.getPipeline();
      pipeline.addBefore("stateMachineParser", "imageDecoder", new ImageDecoder());
      pipeline.addBefore("imageDecoder", "imageRotator", new ImageRotator());
      pipeline.addBefore("imageRotator", "imageEncoder",
          new ImageEncoder(lineLength, endLineBytes)
      );
    }
  }

  private boolean isSupportedImageFormat(String format) {
    String upperCaseFormat = format.toUpperCase();
    for (String inputFormat : Image.InputFormats) {
      if (inputFormat.equals(upperCaseFormat)) {
        return true;
      }
    }
    return false;
  }

  /**
   * this method frees image content.
   * @param ctx context.
   * @param promise promise.
   */
  private void freeImageBuffer(HandlerContext ctx, Promise promise) {
    for (int i = 0; i < this.imageContent.length(); i++) {
      if (i % LINE_LENGTH == 0 && i != 0) {
        ctx.write(END_LINE_BYTES, promise);
      }
      ctx.write(this.imageContent.charAt(i), promise);
    }
    ctx.write(END_LINE_BYTES, promise);
    this.state = States.WAIT_FOR_END_BOUNDARY;
    this.imageContent.setLength(0);
    this.removeImageHandlers(ctx);
  }

  private void removeImageHandlers(HandlerContext ctx) {
    ctx.getPipeline().remove("imageRotator");
    ctx.getPipeline().remove("imageDecoder");
    ctx.getPipeline().remove("imageEncoder");
  }

  public States getState() {
    return state;
  }
}
