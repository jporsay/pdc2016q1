package ar.edu.itba.pdc.mime;

import ar.edu.itba.pdc.net.ChannelInitializer;
import ar.edu.itba.pdc.net.channel.Channel;
import ar.edu.itba.pdc.net.channel.ChannelPipeline;
import ar.edu.itba.pdc.net.channel.handler.EchoHandler;
import ar.edu.itba.pdc.net.codec.ByteBufferToLineQueueDecoder;
import ar.edu.itba.pdc.net.codec.LineToByteBufferEncoder;
import ar.edu.itba.pdc.stats.ByteStatsHandler;

public class MimeParserInitializer extends ChannelInitializer {

  public MimeParserInitializer() {
    super("MIME_TEST_SERVICE");
  }

  protected void initChannel(Channel channel) throws Exception {
    ChannelPipeline pipeline = channel.getPipeline();
    pipeline.addLast("BYTE_STATS", new ByteStatsHandler());
    pipeline.addLast("byteBufferToLine", new ByteBufferToLineQueueDecoder());
    pipeline.addLast("encodeLine", new LineToByteBufferEncoder());
    pipeline.addLast("stateMachineParser", new MimeAutomaton());
    pipeline.addLast("mimeHandler", new MimeHandler());
    pipeline.addLast("echo", new EchoHandler());
    channel.read();
  }
}
