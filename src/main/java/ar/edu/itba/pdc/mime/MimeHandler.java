package ar.edu.itba.pdc.mime;

import ar.edu.itba.pdc.net.async.Promise;
import ar.edu.itba.pdc.net.channel.HandlerContext;
import ar.edu.itba.pdc.net.channel.handler.OutputHandler;

import java.util.Queue;

public class MimeHandler implements OutputHandler {

  @Override
  @SuppressWarnings("unchecked")
  public void write(HandlerContext ctx, Object msg, Promise promise) throws Exception {
    if (!(msg instanceof Queue)) {
      ctx.write(msg);
    }
    Queue<String> messages = (Queue<String>) msg;
    String message = messages.poll();
    while (message != null) {
      ctx.write(message);
      message = messages.poll();
    }
    HandlerContext context = ctx.getPipeline().getContext("stateMachineParser");
    if (context != null && ((MimeAutomaton) context.getHandler()).getState() == States.NO_PARSE) {
      ctx.getPipeline().remove(this);
    }
  }

  @Override
  public void afterRemove(HandlerContext ctx) {
    ctx.getPipeline().remove("stateMachineParser");
    ctx.getPipeline().remove("byteBufferToLine");
  }
}
