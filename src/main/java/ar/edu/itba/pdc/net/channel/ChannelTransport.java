package ar.edu.itba.pdc.net.channel;

import ar.edu.itba.pdc.net.async.Promise;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.channels.SocketChannel;

public interface ChannelTransport {
  SocketChannel getNioChannel();

  SocketAddress localAddress();

  SocketAddress remoteAddress();

  void register(Promise promise);

  void bind(SocketAddress localAddress, Promise promise) throws IOException;

  void connect(SocketAddress remoteAddress, Promise promise);

  void finishConnect();

  void disconnect(Promise promise);

  void close(Promise promise);

  void deregister(Promise promise);

  void beginRead();

  void write(Object msg, Promise promise);

  void read();

  void flush();

  void flushNow();
}
