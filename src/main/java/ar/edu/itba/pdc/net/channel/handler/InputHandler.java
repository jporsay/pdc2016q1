package ar.edu.itba.pdc.net.channel.handler;

import ar.edu.itba.pdc.net.channel.HandlerContext;

public interface InputHandler extends Handler {

  default void channelRegistered(HandlerContext ctx) throws Exception {
    ctx.fireChannelRegistered();
  }

  default void channelUnregistered(HandlerContext ctx) throws Exception {
    ctx.fireChannelUnregistered();
  }

  default void channelRead(HandlerContext ctx, Object msg) throws Exception {
    ctx.fireChannelRead(msg);
  }

  default void channelReadComplete(HandlerContext ctx) throws Exception {
    ctx.fireChannelReadComplete();
  }

  default void exceptionCaught(HandlerContext ctx, Exception cause) throws Exception {
    ctx.fireExceptionCaught(cause);
  }

}
