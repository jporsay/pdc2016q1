package ar.edu.itba.pdc.net;

import ar.edu.itba.pdc.net.async.Future;
import ar.edu.itba.pdc.net.async.Promise;
import ar.edu.itba.pdc.net.channel.Channel;
import ar.edu.itba.pdc.net.channel.ChannelPipeline;
import ar.edu.itba.pdc.net.channel.HandlerContext;
import ar.edu.itba.pdc.net.channel.handler.Handler;
import ar.edu.itba.pdc.net.channel.handler.InputHandler;
import ar.edu.itba.pdc.net.channel.handler.OutputHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.SocketAddress;

class Context implements HandlerContext {
  private static final Logger logger = LoggerFactory.getLogger(HandlerContext.class);
  private volatile Context next;
  private volatile Context prev;
  private String name;
  private ChannelPipeline pipeline;
  private Handler handler;
  private boolean isInput;
  private boolean isOutput;
  private boolean isAdded = false;

  Context(String name, ChannelPipeline pipeline, Handler handler) {
    this.name = name;
    this.pipeline = pipeline;
    this.handler = handler;
    this.isInput = this instanceof InputHandler || handler instanceof InputHandler;
    this.isOutput = this instanceof OutputHandler || handler instanceof OutputHandler;
  }

  @Override
  public void setIsAdded(boolean isAdded) {
    this.isAdded = isAdded;
  }

  Context getPrev() {
    return this.prev;
  }

  void setPrev(Context prev) {
    this.prev = prev;
  }

  Context getNext() {
    return this.next;
  }

  void setNext(Context next) {
    this.next = next;
  }

  @Override
  public String toString() {
    return this.getName();
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public Channel getChannel() {
    return this.pipeline.getChannel();
  }

  @Override
  public Handler getHandler() {
    return this.handler;
  }

  @Override
  public ChannelPipeline getPipeline() {
    return this.pipeline;
  }

  @Override
  public HandlerContext fireChannelRegistered() {
    logger.trace("fireChannelRegistered: {}", this.getName());
    this.getChannel().getTaskLoop().thenRun(
        () -> this.findNextInputContext().invokeChannelRegistered()
    );
    return this;
  }

  @Override
  public HandlerContext fireChannelUnregistered() {
    logger.trace("fireChannelUnregistered: {}", this.getName());
    this.getChannel().getTaskLoop().thenRun(
        () -> this.findNextInputContext().invokeChannelUnregistered()
    );
    return this;
  }

  @Override
  public HandlerContext fireExceptionCaught(final Exception cause) {
    logger.trace("fireExceptionCaught: {}", this.getName());
    this.getChannel().getTaskLoop().thenRun(() -> this.next.invokeExceptionCaught(cause));
    return this;
  }

  @Override
  public HandlerContext fireChannelRead(final Object msg) {
    logger.trace("fireChannelRead: {}", this.getName());
    this.getChannel().getTaskLoop().thenRun(
        () -> this.findNextInputContext().invokeChannelRead(msg)
    ).thenRun(this.getChannel()::read);
    return this;
  }

  @Override
  public HandlerContext fireChannelReadComplete() {
    logger.trace("fireChannelReadComplete: {}", this.getName());
    this.getChannel().getTaskLoop().thenRun(
        () -> this.findNextInputContext().invokeChannelReadComplete()
    );
    return this;
  }

  @Override
  public HandlerContext read() {
    logger.trace("read: {}", this.getName());
    this.getChannel().getTaskLoop().thenRun(
        () -> this.findNextOutputContext().invokeRead()
    );
    return this;
  }

  @Override
  public HandlerContext flush() {
    logger.trace("flush: {}", this.getName());
    this.getChannel().getTaskLoop().thenRun(
        () -> this.findNextOutputContext().invokeFlush()
    );
    return this;
  }

  private void invokeChannelUnregistered() {
    if (this.isAdded) {
      try {
        ((InputHandler) this.getHandler()).channelUnregistered(this);
      } catch (Exception cause) {
        this.fireExceptionCaught(cause);
      }
    } else {
      this.fireChannelUnregistered();
    }
  }

  private void invokeChannelReadComplete() {
    if (this.isAdded) {
      try {
        ((InputHandler) this.getHandler()).channelReadComplete(this);
      } catch (Exception cause) {
        this.fireExceptionCaught(cause);
      }
    } else {
      this.fireChannelReadComplete();
    }
  }

  private void invokeRead() {
    if (this.isAdded) {
      try {
        ((OutputHandler) this.getHandler()).read(this);
      } catch (Exception cause) {
        this.fireExceptionCaught(cause);
      }
    } else {
      this.read();
    }
  }

  private void invokeFlush() {
    if (this.isAdded) {
      try {
        ((OutputHandler) this.getHandler()).flush(this);
      } catch (Exception cause) {
        this.fireExceptionCaught(cause);
      }
    } else {
      this.flush();
    }
  }

  private void invokeChannelRead(Object msg) {
    if (this.isAdded) {
      try {
        ((InputHandler) this.getHandler()).channelRead(this, msg);
      } catch (Exception cause) {
        this.fireExceptionCaught(cause);
      }
    } else {
      this.fireChannelRead(msg);
    }
  }

  private void invokeChannelRegistered() {
    if (this.isAdded) {
      try {
        ((InputHandler) this.getHandler()).channelRegistered(this);
      } catch (Exception cause) {
        this.fireExceptionCaught(cause);
      }
    } else {
      this.fireChannelRegistered();
    }
  }

  private Context findNextInputContext() {
    Context ctx = this;
    do {
      ctx = ctx.next;
    } while (!ctx.isInput);
    return ctx;
  }

  private void invokeExceptionCaught(final Exception cause) {
    if (this.isAdded) {
      try {
        this.getHandler().exceptionCaught(this, cause);
      } catch (Exception ex) {
        logger.error("Cant run exceptionCaught", ex, cause);
      }
    } else {
      this.fireExceptionCaught(cause);
    }
  }

  @Override
  public Promise createPromise() {
    return this.getChannel().createPromise();
  }

  @Override
  public Future bind(SocketAddress localAddress, Promise promise) {
    logger.trace("bind: {}", this.getName());
    this.getChannel().getTaskLoop().thenRun(
        () -> this.findNextOutputContext().invokeBind(localAddress, promise)
    ).thenRun(promise::complete);
    return promise;
  }

  @Override
  public Future connect(SocketAddress remoteAddress, Promise promise) {
    logger.trace("connect: {}", this.getName());
    this.getChannel().getTaskLoop().thenRun(
        () -> this.findNextOutputContext().invokeConnect(remoteAddress, promise)
    ).thenRun(promise::complete);
    return promise;
  }

  @Override
  public Future disconnect(Promise promise) {
    logger.trace("disconnect: {}", this.getName());
    this.getChannel().getTaskLoop().thenRun(
        () -> this.findNextOutputContext().invokeDisonnect(promise)
    ).thenRun(promise::complete);
    return promise;
  }

  @Override
  public Future close(Promise promise) {
    logger.trace("close: {}", this.getName());
    this.getChannel().getTaskLoop().thenRun(
        () -> this.findNextOutputContext().invokeClose(promise)
    ).thenRun(promise::complete);
    return promise;
  }

  @Override
  public Future deregister(Promise promise) {
    logger.trace("deregister: {}", this.getName());
    this.getChannel().getTaskLoop().thenRun(
        () -> this.findNextOutputContext().invokeDeregister(promise)
    ).thenRun(promise::complete);
    return promise;
  }

  @Override
  public Future write(Object msg, Promise promise) {
    logger.trace("write: {}", this.getName());
    this.getChannel().getTaskLoop().thenRun(
        () -> this.findNextOutputContext().invokeWrite(msg, promise)
    ).thenRun(promise::complete);
    return promise;
  }

  @Override
  public Future writeAndFlush(Object msg, Promise promise) {
    logger.trace("writeAndFlush: {}", this.getName());
    this.getChannel().getTaskLoop().thenRun(() -> {
      Context context = this.findNextOutputContext();
      context.invokeWrite(msg, promise);
      context.invokeFlush();
      promise.complete();
    });
    return promise;
  }

  private void invokeDeregister(Promise promise) {
    if (this.isAdded) {
      try {
        ((OutputHandler) this.getHandler()).deregister(this, promise);
      } catch (Exception cause) {
        promise.completeExceptionally(cause);
      }
    } else {
      this.deregister(promise);
    }
  }

  private void invokeWrite(Object msg, Promise promise) {
    if (this.isAdded) {
      try {
        ((OutputHandler) this.getHandler()).write(this, msg, promise);
      } catch (Exception cause) {
        promise.completeExceptionally(cause);
      }
    } else {
      this.write(msg, promise);
    }
  }

  private void invokeClose(Promise promise) {
    if (this.isAdded) {
      try {
        ((OutputHandler) this.getHandler()).close(this, promise);
      } catch (Exception cause) {
        promise.completeExceptionally(cause);
      }
    } else {
      this.close(promise);
    }
  }

  private void invokeDisonnect(Promise promise) {
    if (this.isAdded) {
      try {
        ((OutputHandler) this.getHandler()).disconnect(this, promise);
      } catch (Exception cause) {
        promise.completeExceptionally(cause);
      }
    } else {
      this.disconnect(promise);
    }
  }

  private void invokeConnect(SocketAddress remoteAddress, Promise promise) {
    if (this.isAdded) {
      try {
        ((OutputHandler) this.getHandler()).connect(this, remoteAddress, promise);
      } catch (Exception cause) {
        promise.completeExceptionally(cause);
      }
    } else {
      this.connect(remoteAddress, promise);
    }
  }

  private void invokeBind(SocketAddress localAddress, Promise promise) {
    if (this.isAdded) {
      try {
        ((OutputHandler) this.getHandler()).bind(this, localAddress, promise);
      } catch (Exception cause) {
        promise.completeExceptionally(cause);
      }
    } else {
      this.bind(localAddress, promise);
    }
  }

  private Context findNextOutputContext() {
    Context ctx = this;
    do {
      ctx = ctx.prev;
    } while (!ctx.isOutput);
    return ctx;
  }

}
