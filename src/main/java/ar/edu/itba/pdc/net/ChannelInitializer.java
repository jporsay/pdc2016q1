package ar.edu.itba.pdc.net;

import ar.edu.itba.pdc.net.channel.Channel;
import ar.edu.itba.pdc.net.channel.ChannelPipeline;
import ar.edu.itba.pdc.net.channel.HandlerContext;
import ar.edu.itba.pdc.net.channel.handler.InputHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ChannelInitializer implements InputHandler {
  private static final Logger logger = LoggerFactory.getLogger(Nexus.class);
  private final String name;

  protected ChannelInitializer(String name) {
    this.name = name;
  }

  protected abstract void initChannel(Channel channel) throws Exception;

  public String getName() {
    return this.name;
  }

  @Override
  public final void channelRegistered(HandlerContext ctx) throws Exception {
    this.initChannel(ctx.getChannel());
    ctx.getPipeline().remove(this);
    ctx.getPipeline().fireChannelRegistered();
  }

  @Override
  public void exceptionCaught(HandlerContext ctx, Exception cause) throws Exception {
    logger.warn("INIT CHANNEL FAIL", cause);
    try {
      ChannelPipeline pipeline = ctx.getPipeline();
      if (pipeline.getContext(this) != null) {
        pipeline.remove(this);
      }
    } finally {
      ctx.close();
    }
  }
}
