package ar.edu.itba.pdc.net.channel.handler;

import ar.edu.itba.pdc.net.channel.HandlerContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EchoHandler implements InputHandler {
  private static final Logger logger = LoggerFactory.getLogger(EchoHandler.class);

  @Override
  public void channelRead(HandlerContext ctx, Object msg) throws Exception {
    logger.debug("READ");
    ctx.write(msg);
  }

  @Override
  public void channelReadComplete(HandlerContext ctx) throws Exception {
    logger.debug("READ-COMPLETE");
    ctx.flush();
  }

  @Override
  public void exceptionCaught(HandlerContext ctx, Exception cause) throws Exception {
    logger.error("Unhandled Exception", cause);
    ctx.close();
  }
}
