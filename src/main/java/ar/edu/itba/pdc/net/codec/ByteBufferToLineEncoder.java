package ar.edu.itba.pdc.net.codec;

import ar.edu.itba.pdc.net.channel.HandlerContext;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

public class ByteBufferToLineEncoder extends Encoder<ByteBuffer, Queue<String>> {

  private ByteBufferToLine converter = new ByteBufferToLine();

  @Override
  protected boolean shouldEncode(Object msg) {
    return msg instanceof ByteBuffer;
  }

  @Override
  protected Queue<String> encode(ByteBuffer msg) throws Exception {
    Queue<String> outputBuffer = new LinkedBlockingDeque<>();
    this.converter.convert(msg, outputBuffer);
    return outputBuffer;
  }

  @Override
  public void beforeRemove(HandlerContext ctx) {
    if (this.converter.hasPartialLine()) {
      ctx.write(ByteBuffer.wrap(
          this.converter.getPartial().getBytes(Charset.forName("US-ASCII"))
      ).compact());
    }
  }
}
