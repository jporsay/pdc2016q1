package ar.edu.itba.pdc.net.codec;

import ar.edu.itba.pdc.net.channel.HandlerContext;
import ar.edu.itba.pdc.net.channel.handler.InputHandler;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

public abstract class Decoder<I,O> implements InputHandler {
  private final Queue<O> outputBuffer = new LinkedBlockingDeque<>();

  @Override
  @SuppressWarnings("unchecked")
  public void channelRead(HandlerContext ctx, Object msg) throws Exception {
    if (this.shouldDecode(msg)) {
      this.decode((I)msg, this.outputBuffer);
      O out = this.outputBuffer.poll();
      while (out != null) {
        ctx.fireChannelRead(out);
        out = this.outputBuffer.poll();
      }
    } else {
      ctx.fireChannelRead(msg);
    }
  }

  /**
   * Just check if the msg is of type I.
   * @param msg message to check.
   * @return TRUE if the msg is of type I.
   */
  protected abstract boolean shouldDecode(Object msg);

  /**
   *  Decode from I to O.
   *  Remember to store somewhere that partial content.
   * @param msg message to decode.
   */
  protected abstract void decode(I msg, Queue<O> out);
}
