package ar.edu.itba.pdc.net.codec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class LineToByteBufferEncoder extends Encoder<String, ByteBuffer> {
  private static final Logger logger = LoggerFactory.getLogger(LineToByteBufferEncoder.class);
  private static final Charset charset = Charset.forName("US-ASCII");

  @Override
  protected boolean shouldEncode(Object msg) {
    return msg instanceof String;
  }

  @Override
  protected ByteBuffer encode(String msg) {
    logger.debug("Encoded: {}", msg);
    return ByteBuffer.wrap((msg + "\r\n").getBytes(charset)).compact();
  }
}
