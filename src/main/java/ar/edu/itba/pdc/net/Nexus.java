package ar.edu.itba.pdc.net;

import ar.edu.itba.pdc.net.channel.Channel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

public class Nexus implements Reactor, Runnable {
  private static final Logger logger = LoggerFactory.getLogger(Nexus.class);
  private final Selector selector;
  private final long timeoutMs;
  private Thread thread;

  public Nexus(long timeoutMs) throws IOException {
    this.timeoutMs = timeoutMs;
    this.selector = Selector.open();
  }

  @Override
  public void run() {
    this.thread = Thread.currentThread();
    while (this.selector.isOpen()) {
      try {
        if (this.selector.select(this.timeoutMs) == 0) {
          this.handleTimeout();
          continue;
        }
      } catch (IOException ex) {
        logger.error("select ERROR", ex);
        break;
      }
      Iterator<SelectionKey> keyIter = this.selector.selectedKeys().iterator();
      while (keyIter.hasNext()) {
        SelectionKey key = keyIter.next();
        try {
          if (key.isValid()) {
            this.dispatch(key);
          }
        } catch (Exception cause) {
          logger.error("Could not dispatch", cause);
        } finally {
          keyIter.remove();
        }
      }
      if (Thread.interrupted()) {
        this.handleInterrupt();
      }
    }
    logger.info("selector CLOSED");
  }

  private void handleTimeout() {
    logger.trace("IO TIMEOUT");
  }

  private void handleInterrupt() {
    logger.info("INTERRUPTED");
    try {
      this.selector.close();
    } catch (IOException cause) {
      logger.error("Could not close selector", cause);
    }
  }

  private void dispatch(SelectionKey key) throws Exception {
    if (!key.isValid()) {
      this.handleInterrupt();
    } else if (key.isAcceptable()) {
      ((Probe) key.attachment()).handleAccept(key);
    } else {
      Channel channel = (Channel) key.attachment();
      if (key.isReadable()) {
        channel.getTransport().read();
      }
      if (key.isWritable()) {
        channel.getTransport().flushNow();
      }
      if (key.isConnectable()) {
        channel.getTransport().finishConnect();
      }
    }
  }

  @Override
  public void bind(InetSocketAddress endpoint, ChannelInitializer initializer)
      throws IOException {
    ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
    serverSocketChannel.configureBlocking(false);
    serverSocketChannel.socket().bind(endpoint);
    logger.info("{} waiting connections @ {}", initializer.getName(), endpoint);
    Probe probe = new Probe(this, initializer);
    serverSocketChannel.register(this.selector, SelectionKey.OP_ACCEPT, probe);
  }

  @Override
  public void connect(InetSocketAddress endpoint, ChannelInitializer initializer)
      throws IOException {
    Gateway channel = new Gateway(SocketChannel.open(), this);
    channel.getPipeline().addLast("[INIT]", initializer);
    channel.getTransport().connect(endpoint, channel.createPromise());
    logger.info("Connecting to @ {}", endpoint);
  }

  @Override
  public Selector getSelector() {
    return this.selector;
  }

  @Override
  public boolean runningInReactor(Thread thread) {
    return this.thread == thread;
  }

  private static class Probe implements Acceptor {

    private final Nexus nexus;
    private final ChannelInitializer initializer;

    Probe(Nexus nexus, ChannelInitializer initializer) {
      this.nexus = nexus;
      this.initializer = initializer;
    }

    public void handleAccept(SelectionKey key) throws Exception {
      SocketChannel clientChannel = ((ServerSocketChannel) key.channel()).accept();
      logger.info("Connection accepted");
      clientChannel.configureBlocking(false);
      Gateway channel = new Gateway(clientChannel, this.nexus);
      channel.getPipeline().addLast("[INIT]", this.initializer);
      channel.getTransport().register(channel.createPromise());
    }
  }

}
