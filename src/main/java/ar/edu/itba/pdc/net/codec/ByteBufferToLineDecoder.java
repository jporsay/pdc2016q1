package ar.edu.itba.pdc.net.codec;

import java.nio.ByteBuffer;
import java.util.Queue;

public class ByteBufferToLineDecoder extends Decoder<ByteBuffer, String> {

  private ByteBufferToLine converter = new ByteBufferToLine();

  @Override
  protected boolean shouldDecode(Object msg) {
    return msg instanceof ByteBuffer;
  }

  @Override
  protected void decode(ByteBuffer msg, Queue<String> out) {
    this.converter.convert(msg, out);
  }
}
