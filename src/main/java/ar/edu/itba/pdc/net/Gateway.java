package ar.edu.itba.pdc.net;

import ar.edu.itba.pdc.net.async.Future;
import ar.edu.itba.pdc.net.async.Promise;
import ar.edu.itba.pdc.net.async.TaskLoop;
import ar.edu.itba.pdc.net.channel.Channel;
import ar.edu.itba.pdc.net.channel.ChannelPipeline;

import java.net.SocketAddress;
import java.nio.channels.SocketChannel;

class Gateway implements Channel {

  private final Transport transport;
  private final ChannelPipeline pipeline;
  private final TaskLoop taskLoop;
  private final Nexus nexus;

  Gateway(SocketChannel javaChannel, Nexus nexus) {
    this.pipeline = new Pipeline(this);
    this.transport = new Transport(this, javaChannel);
    this.nexus = nexus;
    this.taskLoop = new TaskLoop(this.nexus);
  }

  @Override
  public SocketAddress localAddress() {
    return this.transport.localAddress();
  }

  @Override
  public SocketAddress remoteAddress() {
    return this.transport.remoteAddress();
  }

  @Override
  public ChannelPipeline getPipeline() {
    return this.pipeline;
  }

  @Override
  public Promise createPromise() {
    return new Promise(this.nexus);
  }

  @Override
  public Transport getTransport() {
    return this.transport;
  }

  public TaskLoop getTaskLoop() {
    return this.taskLoop;
  }

  @Override
  public Reactor getReactor() {
    return this.nexus;
  }


  @Override
  public Future bind(SocketAddress localAddress, Promise promise) {
    return this.getPipeline().bind(localAddress, promise);
  }

  @Override
  public Future connect(SocketAddress remoteAddress, Promise promise) {
    return this.getPipeline().connect(remoteAddress, promise);
  }

  @Override
  public Future disconnect(Promise promise) {
    return this.getPipeline().disconnect(promise);
  }

  @Override
  public Future close(Promise promise) {
    return this.getPipeline().close(promise);
  }

  @Override
  public Future deregister(Promise promise) {
    return this.getPipeline().deregister(promise);
  }

  @Override
  public Channel read() {
    this.getPipeline().read();
    return this;
  }

  @Override
  public Future write(Object msg, Promise promise) {
    return this.getPipeline().write(msg, promise);
  }

  @Override
  public Channel flush() {
    this.getPipeline().flush();
    return this;
  }

  @Override
  public Future writeAndFlush(Object msg, Promise promise) {
    return this.getPipeline().writeAndFlush(msg, promise);
  }

}
