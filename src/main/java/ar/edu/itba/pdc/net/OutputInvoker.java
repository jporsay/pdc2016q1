package ar.edu.itba.pdc.net;

import ar.edu.itba.pdc.net.async.Future;
import ar.edu.itba.pdc.net.async.Promise;

import java.net.SocketAddress;

/**
 * A set of Output commands. Since output commands don't finish instantly, nor we want to block,
 * they return {@link Future} and receive {@link Promise} to chain workload to them.
 */
public interface OutputInvoker {

  /**
   * Creates a new promise.
   * @return .
   */
  Promise createPromise();

  /**
   * Calls {@link #bind(SocketAddress, Promise)} with a default promise.
   * @param localAddress the local address to bind to.
   * @return future for chaining.
   */
  default Future bind(SocketAddress localAddress) {
    return this.bind(localAddress, this.createPromise());
  }

  /**
   * Invokes a bind.
   * @param localAddress address to bind to.
   * @param promise that will be notified on Success or Error.
   * @return future for chaining.
   */
  Future bind(SocketAddress localAddress, Promise promise);

  /**
   * Calls {@link #connect(SocketAddress, Promise)} with a default promise.
   * @param remoteAddress the remote address to connect to.
   * @return future for chaining.
   */
  default Future connect(SocketAddress remoteAddress) {
    return this.connect(remoteAddress, this.createPromise());
  }

  /**
   * Invokes a connect.
   * @param remoteAddress address to connect to.
   * @param promise that will be notified on Success or Error.
   * @return future for chaining.
   */
  Future connect(SocketAddress remoteAddress, Promise promise);

  /**
   * Calls {@link #disconnect(Promise)} with a default promise.
   * @return future for chaining.
   */
  default Future disconnect() {
    return this.disconnect(this.createPromise());
  }

  /**
   * Invokes a disconnect.
   * @param promise that will be notified on Success or Error.
   * @return future for chaining.
   */
  Future disconnect(Promise promise);

  /**
   * Calls {@link #bind(SocketAddress, Promise)} with a default promise.
   * @return future for chaining.
   */
  default Future close() {
    return this.close(this.createPromise());
  }

  /**
   * Invokes a close.
   * @param promise that will be notified on Success or Error.
   * @return future for chaining.
   */
  Future close(Promise promise);

  /**
   * Calls {@link #deregister(Promise)} with a default promise.
   * @return future for chaining.
   */
  default Future deregister() {
    return this.deregister(this.createPromise());
  }

  /**
   * Invokes a deregister.
   * @param promise that will be notified on Success or Error.
   * @return future for chaining.
   */
  Future deregister(Promise promise);

  /**
   * Calls {@link #write(Object, Promise)} with a default promise.
   * @param msg message to write.
   * @return future for chaining.
   */
  default Future write(Object msg) {
    return this.write(msg, this.createPromise());
  }

  /**
   * Invokes a write.
   * @param msg the message to write.
   * @param promise that will be notified on Success or Error.
   * @return future for chaining.
   */
  Future write(Object msg, Promise promise);

  /**
   * Calls {@link #writeAndFlush(Object, Promise)} with a default promise.
   * @param msg message to write.
   * @return future for chaining.
   */
  default Future writeAndFlush(Object msg) {
    return this.writeAndFlush(msg, this.createPromise());
  }

  /**
   * Invokes a write and then a flush.
   * @param msg the message to write.
   * @param promise that will be notified on Success or Error.
   * @return future for chaining.
   */
  Future writeAndFlush(Object msg, Promise promise);

  /**
   * Invoke reads.
   * @return this for chaining.
   */
  OutputInvoker read();

  /**
   * Invoke flush of all written messages.
   * @return this for chaining.
   */
  OutputInvoker flush();

}

