package ar.edu.itba.pdc.net.codec;

import ar.edu.itba.pdc.net.channel.HandlerContext;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.Queue;

public class ByteBufferToLineQueueDecoder extends Decoder<ByteBuffer, Queue<String>> {

  private ByteBufferToLine converter = new ByteBufferToLine();

  @Override
  protected boolean shouldDecode(Object msg) {
    return msg instanceof ByteBuffer;
  }

  @Override
  protected void decode(ByteBuffer msg, Queue<Queue<String>> out) {
    Queue<String> queue = new LinkedList<>();
    this.converter.convert(msg, queue);
    out.add(queue);
  }

  @Override
  public void beforeRemove(HandlerContext ctx) {
    if (this.converter.hasPartialLine()) {
      ctx.write(ByteBuffer.wrap(
          this.converter.getPartial().getBytes(Charset.forName("US-ASCII"))
      ).compact());
    }
  }
}
