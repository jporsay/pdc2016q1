package ar.edu.itba.pdc.net.codec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.Queue;

public class ByteBufferToLine {
  private static final Logger logger = LoggerFactory.getLogger(ByteBufferToLine.class);
  private StringBuilder lineBuilder = new StringBuilder();

  /**
   * Process msg and add to out each line finishing in \r\n.
   * @param msg Raw char byte buffer
   * @param out Processed lines.
   */
  public void convert(ByteBuffer msg, Queue<String> out) {
    char current;
    char last = 0;
    msg.flip();
    while (msg.hasRemaining()) {
      current = (char) msg.get();
      if (last == '\r' && current == '\n') {
        if (this.lineBuilder.length() > 0) {
          this.lineBuilder.deleteCharAt(this.lineBuilder.length() - 1);
        }
        String line = this.lineBuilder.toString();
        this.lineBuilder.setLength(0);
        out.add(line);
        logger.debug("Decoded: {}", line);
      } else {
        this.lineBuilder.append(current);
      }
      last = current;
    }
    msg.compact();
  }

  public boolean hasPartialLine() {
    return this.lineBuilder.length() > 0;
  }

  public String getPartial() {
    return this.lineBuilder.toString();
  }
}
