package ar.edu.itba.pdc.net.channel.handler;

import ar.edu.itba.pdc.net.async.Promise;
import ar.edu.itba.pdc.net.channel.HandlerContext;

import java.net.SocketAddress;

public interface OutputHandler extends Handler {

  default void bind(HandlerContext ctx, SocketAddress localAddress, Promise promise)
      throws Exception {
    ctx.bind(localAddress, promise);
  }

  default void connect(HandlerContext ctx, SocketAddress remoteAddress, Promise promise)
      throws Exception {
    ctx.connect(remoteAddress, promise);
  }

  default void disconnect(HandlerContext ctx, Promise promise)
      throws Exception {
    ctx.disconnect(promise);
  }

  default void close(HandlerContext ctx, Promise promise)
      throws Exception {
    ctx.close(promise);
  }

  default void deregister(HandlerContext ctx, Promise promise)
      throws Exception {
    ctx.deregister(promise);
  }

  default void read(HandlerContext ctx)
      throws Exception {
    ctx.read();
  }

  default void write(HandlerContext ctx, Object msg, Promise promise)
      throws Exception {
    ctx.write(msg, promise);
  }

  default void flush(HandlerContext ctx)
      throws Exception {
    ctx.flush();
  }
}
