package ar.edu.itba.pdc.net.channel.handler;

import ar.edu.itba.pdc.net.channel.HandlerContext;

/**
 * A Handler is what makes.
 */
public interface Handler {

  default void exceptionCaught(HandlerContext ctx, Exception cause) throws Exception {
    ctx.fireExceptionCaught(cause);
  }

  default void afterRemove(HandlerContext ctx) {
    // Nothing to do
  }

  default void beforeRemove(HandlerContext ctx) {
    // Nothing to do
  }
}
