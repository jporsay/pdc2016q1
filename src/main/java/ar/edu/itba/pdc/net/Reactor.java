package ar.edu.itba.pdc.net;

import ar.edu.itba.pdc.net.channel.HandlerContext;
import ar.edu.itba.pdc.net.channel.handler.InputHandler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;

/**
 * This class manages all NIO's Reactor (Selector) behaviour.
 */
public interface Reactor {

  /**
   * Creates a {@link java.nio.channels.ServerSocketChannel} and binds it
   * at {@link InetSocketAddress endpoint} for Incoming Connections.
   * @param endpoint  the ip and port from where to listen.
   * @param initializer an instance of {@link InputHandler} to manage initialization
   *                    at {@link InputHandler#channelRegistered(HandlerContext) call}.
   * @throws IOException in I/O error.
   */
  void bind(InetSocketAddress endpoint, ChannelInitializer initializer) throws IOException;

  /**
   * Creates a {@link java.nio.channels.SocketChannel} and connects it
   * to {@link InetSocketAddress endpoint} for I/O.
   * @param endpoint the ip and port to connect.
   * @param initializer an instance of {@link InputHandler} to manage initialization
   *                    at {@link InputHandler#channelRegistered(HandlerContext) call}.
   * @throws IOException on I/O error.
   */
  void connect(InetSocketAddress endpoint, ChannelInitializer initializer) throws IOException;

  /**
   * Returns the final static {@link java.nio.channels.Selector} instance of this Nexus.
   * @return selector.
   */
  Selector getSelector();


  boolean runningInReactor(Thread thread);

  /**
   * The Acceptor is a temporary class that will handle new incoming connections to the
   * listening {@link java.net.InetAddress}'s {@link java.nio.channels.ServerSocketChannel}.
   */
  interface Acceptor {

    /**
     * Creates a {@link java.nio.channels.SocketChannel} once the connection is accepted
     * to {@link InetSocketAddress endpoint} for I/O.
     * @param key triggered {@link SelectionKey}.
     * @throws Exception on I/O or Implementation error.
     */
    void handleAccept(SelectionKey key) throws Exception;
  }

}
