package ar.edu.itba.pdc.net;

import ar.edu.itba.pdc.net.async.Future;
import ar.edu.itba.pdc.net.async.Promise;
import ar.edu.itba.pdc.net.channel.Channel;
import ar.edu.itba.pdc.net.channel.ChannelPipeline;
import ar.edu.itba.pdc.net.channel.HandlerContext;
import ar.edu.itba.pdc.net.channel.handler.Handler;
import ar.edu.itba.pdc.net.channel.handler.InputHandler;
import ar.edu.itba.pdc.net.channel.handler.OutputHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.SocketAddress;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentHashMap;

// TODO: pipeline changes should be done inside the event loop.
class Pipeline implements ChannelPipeline {
  private static final Logger logger = LoggerFactory.getLogger(Pipeline.class);
  private final Channel channel;
  private final Context head;
  private final Context tail;
  private final Map<String, Context> names;

  Pipeline(Channel channel) {
    this.channel = channel;
    this.tail = new TailContext(this);
    this.head = new HeadContext(this);
    this.head.setNext(this.tail);
    this.tail.setPrev(this.head);
    this.head.setIsAdded(true);
    this.tail.setIsAdded(true);
    this.names = new ConcurrentHashMap<>();
  }

  public Channel getChannel() {
    return this.channel;
  }

  @Override
  public Promise createPromise() {
    return this.getChannel().createPromise();
  }

  @Override
  public boolean hasContext(String name) {
    return this.names.containsKey(name);
  }

  private Context createNewContext(String name, Handler handler) {
    Context newContext = new Context(name, this, handler);
    if (this.names.containsKey(name)) {
      logger.error("handler name \"{}\" in use", name);
    }
    this.names.put(name, newContext);
    return newContext;
  }

  private void addBefore(Context baseCtx, Context newCtx) {
    Context prev = baseCtx.getPrev();
    newCtx.setPrev(prev);
    newCtx.setNext(baseCtx);
    prev.setNext(newCtx);
    baseCtx.setPrev(newCtx);
    newCtx.setIsAdded(true);
    logger.debug("Context {} added before {}.", newCtx, baseCtx);
  }

  @Override
  public ChannelPipeline addBefore(String baseName, String name, Handler handler) {
    this.getChannel().getTaskLoop().thenRun(() -> {
      Context baseCtx = (Context) this.getContext(baseName);
      Context newCtx = this.createNewContext(name, handler);
      this.addBefore(baseCtx, newCtx);
    });
    return this;
  }

  @Override
  public ChannelPipeline addLast(String name, Handler handler) {
    this.getChannel().getTaskLoop().thenRun(() -> {
      Context newCtx = this.createNewContext(name, handler);
      this.addBefore(this.tail, newCtx);
    });
    return this;
  }

  private void addAfter(Context baseCtx, Context newCtx) {
    Context next = baseCtx.getNext();
    newCtx.setNext(next);
    newCtx.setPrev(baseCtx);
    next.setPrev(newCtx);
    baseCtx.setNext(newCtx);
    newCtx.setIsAdded(true);
    logger.debug("Context {} added after {}.", newCtx, baseCtx);
  }

  @Override
  public ChannelPipeline addAfter(Handler base, String name, Handler handler) {
    this.getChannel().getTaskLoop().thenRun(() -> {
      Context newCtx = this.createNewContext(name, handler);
      Context baseCtx = (Context) this.getContext(base);
      this.addAfter(baseCtx, newCtx);
    });
    return this;
  }

  @Override
  public ChannelPipeline addAfter(String baseName, String name, Handler handler) {
    this.getChannel().getTaskLoop().thenRun(() -> {
      Context baseCtx = (Context) this.getContext(baseName);
      this.addAfter(baseCtx.getHandler(), name, handler);
    });
    return this;
  }

  @Override
  public ChannelPipeline addFirst(String name, Handler handler) {
    this.getChannel().getTaskLoop().thenRun(() -> {
      Context newCtx = this.createNewContext(name, handler);
      this.addAfter(this.head, newCtx);
    });
    return this;
  }

  private void remove(Context ctx) {
    if (ctx == null) {
      return;
    }
    ctx.getHandler().beforeRemove(ctx);
    ctx.getPrev().setNext(ctx.getNext());
    ctx.getNext().setPrev(ctx.getPrev());
    this.names.remove(ctx.getName());
    ctx.getHandler().afterRemove(ctx);
    ctx.setIsAdded(false);
    logger.debug("Context {} removed.", ctx);
  }

  @Override
  public Handler remove(String name) {
    this.getChannel().getTaskLoop().thenRun(() -> {
      Context ctx = (Context) this.getContext(name);
      this.remove(ctx);
    });
    return null;
  }

  @Override
  public Handler remove(Handler handler) {
    this.getChannel().getTaskLoop().thenRun(() -> {
      Context ctx = (Context) this.getContext(handler);
      this.remove(ctx);
    });
    return handler;
  }

  @Override
  public Handler replace(String oldName, String newName, Handler newHandler) {
    this.getChannel().getTaskLoop().thenRun(() -> {
      this.addBefore(oldName, newName, newHandler);
      this.remove(oldName);
    });
    return newHandler;
  }

  @Override
  public Handler getFirst() {
    HandlerContext ctx = this.getFirstContext();
    if (ctx == null) {
      return null;
    } else {
      return ctx.getHandler();
    }
  }

  @Override
  public HandlerContext getFirstContext() {
    HandlerContext ctx = this.head.getNext();
    if (ctx == this.tail) {
      return null;
    } else {
      return ctx;
    }
  }

  @Override
  public Handler getLast() {
    HandlerContext ctx = this.getLastContext();
    if (ctx == null) {
      return null;
    } else {
      return ctx.getHandler();
    }
  }

  @Override
  public HandlerContext getLastContext() {
    HandlerContext ctx = this.tail.getPrev();
    if (ctx == this.head) {
      return null;
    } else {
      return ctx;
    }
  }

  @Override
  public Handler get(String name) {
    return this.getContext(name).getHandler();
  }

  @Override
  public HandlerContext getContext(String name) {
    if (!this.names.containsKey(name)) {
      logger.error("handler name \"{}\" doesn't exist", name);
    }
    return this.names.get(name);
  }

  @Override
  public HandlerContext getContext(Handler handler) {
    Context ctx = this.head.getNext();
    while (ctx != this.tail) {
      if (ctx.getHandler() == handler) {
        return ctx;
      }
      ctx = ctx.getNext();
    }
    throw new NoSuchElementException("handler");
  }

  @Override
  public ChannelPipeline fireChannelRegistered() {
    this.head.fireChannelRegistered();
    return this;
  }

  @Override
  public ChannelPipeline fireChannelUnregistered() {
    this.head.fireChannelUnregistered();
    return this;
  }

  @Override
  public ChannelPipeline fireExceptionCaught(Exception cause) {
    this.head.fireExceptionCaught(cause);
    return this;
  }

  @Override
  public ChannelPipeline fireChannelRead(Object msg) {
    this.head.fireChannelRead(msg);
    return this;
  }

  @Override
  public ChannelPipeline fireChannelReadComplete() {
    this.head.fireChannelReadComplete();
    return this;
  }

  @Override
  public Future bind(SocketAddress localAddress, Promise promise) {
    return this.tail.bind(localAddress, promise);
  }

  @Override
  public Future connect(SocketAddress remoteAddress, Promise promise) {
    return this.tail.connect(remoteAddress, promise);
  }

  @Override
  public Future disconnect(Promise promise) {
    return this.tail.disconnect(promise);
  }

  @Override
  public Future close(Promise promise) {
    return this.tail.close(promise);
  }

  @Override
  public Future deregister(Promise promise) {
    return this.tail.deregister(promise);
  }

  @Override
  public ChannelPipeline flush() {
    this.tail.flush();
    return this;
  }

  @Override
  public ChannelPipeline read() {
    this.tail.read();
    return this;
  }

  @Override
  public Future write(Object msg, Promise promise) {
    return this.tail.write(msg, promise);
  }

  @Override
  public Future writeAndFlush(Object msg, Promise promise) {
    return this.tail.writeAndFlush(msg, promise);
  }

  private static final class TailContext extends Context implements InputHandler {

    TailContext(ChannelPipeline pipeline) {
      super("[TAIL]", pipeline, null);
    }

    @Override
    public Handler getHandler() {
      return this;
    }

    @Override
    public void exceptionCaught(HandlerContext ctx, Exception cause) throws Exception {
      logger.error("Unhandled Input Exception", cause);
      throw cause;
    }

    @Override
    public void channelRegistered(HandlerContext ctx) throws Exception {
      // NOOP
    }

    @Override
    public void channelUnregistered(HandlerContext ctx) throws Exception {
      // NOOP
    }

    @Override
    public void channelRead(HandlerContext ctx, Object msg) throws Exception {
      logger.warn("Unhandled Input Message: {}", msg);
    }

    @Override
    public void channelReadComplete(HandlerContext ctx) throws Exception {
      // NOOP
    }

  }

  private static final class HeadContext extends Context implements OutputHandler, InputHandler {

    HeadContext(ChannelPipeline pipeline) {
      super("[HEAD]", pipeline, null);
    }

    @Override
    public Handler getHandler() {
      return this;
    }

    @Override
    public void channelRegistered(HandlerContext ctx) throws Exception {
      logger.trace("channelRegistered | HEAD");
      ctx.fireChannelRegistered();
    }

    @Override
    public void channelUnregistered(HandlerContext ctx) throws Exception {
      logger.trace("channelUnregistered | HEAD");
      ctx.fireChannelUnregistered();
    }

    @Override
    public void channelRead(HandlerContext ctx, Object msg) throws Exception {
      logger.trace("channelRead | HEAD");
      ctx.fireChannelRead(msg);
    }

    @Override
    public void channelReadComplete(HandlerContext ctx) throws Exception {
      logger.trace("channelReadComplete | HEAD");
      ctx.fireChannelReadComplete();
    }

    @Override
    public void exceptionCaught(HandlerContext ctx, Exception cause) throws Exception {
      logger.trace("exceptionCaught | HEAD");
      ctx.fireExceptionCaught(cause);
    }

    @Override
    public void bind(HandlerContext ctx, SocketAddress localAddress, Promise promise)
        throws Exception {
      logger.trace("bind | HEAD");
      this.getChannel().getTransport().bind(localAddress, promise);
    }

    @Override
    public void connect(HandlerContext ctx, SocketAddress remoteAddress, Promise promise)
        throws Exception {
      logger.trace("connect | HEAD");
      this.getChannel().getTransport().connect(remoteAddress, promise);
    }

    @Override
    public void disconnect(HandlerContext ctx, Promise promise) throws Exception {
      logger.trace("disconnect | HEAD");
      this.getChannel().getTransport().disconnect(promise);
    }

    @Override
    public void close(HandlerContext ctx, Promise promise) throws Exception {
      logger.trace("close | HEAD");
      this.getChannel().getTransport().close(promise);
    }

    @Override
    public void deregister(HandlerContext ctx, Promise promise) throws Exception {
      logger.trace("deregister | HEAD");
      this.getChannel().getTransport().deregister(promise);
    }

    @Override
    public void read(HandlerContext ctx) throws Exception {
      logger.trace("read | HEAD");
      this.getChannel().getTransport().beginRead();
    }

    @Override
    public void write(HandlerContext ctx, Object msg, Promise promise) throws Exception {
      logger.trace("write | HEAD");
      this.getChannel().getTransport().write(msg, promise);
    }

    @Override
    public void flush(HandlerContext ctx) throws Exception {
      logger.trace("flush | HEAD");
      this.getChannel().getTransport().flush();
    }

  }
}
