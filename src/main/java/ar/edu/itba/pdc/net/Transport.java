package ar.edu.itba.pdc.net;

import ar.edu.itba.pdc.net.async.Promise;
import ar.edu.itba.pdc.net.channel.Channel;
import ar.edu.itba.pdc.net.channel.ChannelPipeline;
import ar.edu.itba.pdc.net.channel.ChannelTransport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

class Transport implements ChannelTransport {
  private static final Logger logger = LoggerFactory.getLogger(Transport.class);
  private static final int CAPACITY = 1024;
  private final Channel channel;
  private final ByteBuffer readBuffer;
  private final Queue<ByteBuffer> writeBuffers = new LinkedBlockingDeque<>();
  private SocketChannel nioChannel;
  private SelectionKey selectionKey;
  private boolean clientDisconnected = false;
  private Promise connectPromise;


  Transport(Channel channel, SocketChannel nioChannel) {
    logger.debug("New Transport");
    this.channel = channel;
    this.nioChannel = nioChannel;
    this.readBuffer = ByteBuffer.allocateDirect(CAPACITY);
  }

  @Override
  public SocketChannel getNioChannel() {
    return this.nioChannel;
  }

  @Override
  public SocketAddress localAddress() {
    return this.getNioChannel().socket().getLocalSocketAddress();
  }

  @Override
  public SocketAddress remoteAddress() {
    return this.getNioChannel().socket().getRemoteSocketAddress();
  }

  @Override
  public void register(Promise promise) {
    logger.trace("REGISTER");
    try {
      this.selectionKey = this.getNioChannel().register(
          this.channel.getReactor().getSelector(), 0, this.channel
      );
      promise.complete();
      this.getPipeline().fireChannelRegistered();
    } catch (ClosedChannelException cause) {
      promise.completeExceptionally(cause);
    }
  }

  @Override
  public void bind(SocketAddress localAddress, Promise promise) {
    try {
      this.getNioChannel().bind(localAddress);
      promise.complete();
    } catch (IOException cause) {
      promise.completeExceptionally(cause);
    }
  }

  @Override
  public void connect(SocketAddress remoteAddress, Promise promise) {
    if (this.connectPromise != null) {
      promise.completeExceptionally(new Exception("Already trying to connect"));
    }
    this.connectPromise = promise;
    try {
      this.getNioChannel().configureBlocking(false);
      this.getNioChannel().connect(remoteAddress);
      this.selectionKey = this.getNioChannel().register(
          this.channel.getReactor().getSelector(), 0, this.channel
      );
      this.addInterest(SelectionKey.OP_CONNECT);
    } catch (IOException cause) {
      promise.completeExceptionally(cause);
    }
  }

  @Override
  public void finishConnect() {
    logger.trace("finishConnect");
    this.removeInterest(SelectionKey.OP_CONNECT);
    try {
      this.getNioChannel().finishConnect();
      this.connectPromise.complete();
      this.getPipeline().fireChannelRegistered();
    } catch (IOException cause) {
      this.connectPromise.completeExceptionally(cause);
    }
  }

  @Override
  public void disconnect(Promise promise) {
    this.close(promise);
  }

  @Override
  @SuppressWarnings("unchecked")
  public void close(Promise promise) {
    logger.trace("CLOSE");
    try {
      this.getNioChannel().close();
      promise.complete();
    } catch (IOException cause) {
      promise.completeExceptionally(cause);
    }
  }

  @Override
  @SuppressWarnings("unchecked")
  public void deregister(Promise promise) {
    this.selectionKey.cancel();
    promise.complete();
  }

  @Override
  public void beginRead() {
    logger.trace("BEGIN-READ");
    this.addInterest(SelectionKey.OP_READ);
  }

  @Override
  public void write(Object msg, Promise promise) {
    logger.trace("WRITE");
    this.writeBuffers.add((ByteBuffer) msg);
    promise.complete();
  }

  @Override
  public void read() {
    logger.trace("read");
    int bytesRead = 0;
    try {
      bytesRead = this.nioChannel.read(this.readBuffer);
    } catch (IOException cause) {
      this.getPipeline().fireExceptionCaught(cause);
    }
    if (bytesRead == -1) {
      this.getPipeline().fireChannelReadComplete();
      this.clientDisconnected();
    } else if (bytesRead > 0) {
      this.getPipeline().fireChannelRead(this.readBuffer);
    }
    this.getPipeline().fireChannelReadComplete();
  }

  @Override
  public void flush() {
    logger.trace("BEGIN FLUSH");
    if (this.writeBuffers.size() > 0) {
      try {
        this.addInterest(SelectionKey.OP_WRITE);
      } catch (CancelledKeyException cause) {
        flushNow();
      }
    }
  }

  @Override
  public void flushNow() {
    ByteBuffer buffer = this.writeBuffers.peek();
    if (buffer != null) {
      try {
        buffer.flip();
        this.nioChannel.write(buffer);
        if (!buffer.hasRemaining()) {
          this.writeBuffers.remove();
        }
        buffer.compact();
      } catch (IOException cause) {
        this.getPipeline().fireExceptionCaught(cause);
      }
    } else {
      logger.trace("NO MORE FLUSHES {}", this.clientDisconnected);
      this.removeInterest(SelectionKey.OP_WRITE);
      if (this.clientDisconnected) {
        logger.info("Client Disconnected AND FLUSHED");
        this.getPipeline().close();
      }
    }
  }

  private void clientDisconnected() {
    logger.info("CLIENT DISCONNECTED");
    this.removeInterest(SelectionKey.OP_READ);
    this.clientDisconnected = true;
  }

  private void addInterest(int interestOp) {
    logger.trace("+Interest {}", interestOp);
    final int interestOps = this.selectionKey.interestOps();
    this.selectionKey.interestOps(interestOps | interestOp);
    this.selectionKey.selector().wakeup();
  }

  private void removeInterest(int interestOp) {
    logger.trace("-Interest {}", interestOp);
    final int interestOps = this.selectionKey.interestOps();
    this.selectionKey.interestOps(interestOps & ~interestOp);
  }

  private ChannelPipeline getPipeline() {
    return this.channel.getPipeline();
  }
}
