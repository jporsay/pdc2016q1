package ar.edu.itba.pdc.net.channel;

import ar.edu.itba.pdc.net.OutputInvoker;
import ar.edu.itba.pdc.net.Reactor;
import ar.edu.itba.pdc.net.async.TaskLoop;

import java.net.SocketAddress;

/**
 * For every connection established with a client a new {@link Channel} is created
 * Apart from the Channel, each connection have it's own {@link ChannelPipeline},
 * {@link TaskLoop} and {@link ChannelTransport}.
 * {@link Channel} also implements {@link OutputInvoker} because you can run any Output
 * Command to it i.e. {@link OutputInvoker#write(Object)}.
 */
public interface Channel extends OutputInvoker {

  /**
   * The local SocketAddress where this Channel is bounded.
   * @return .
   */
  SocketAddress localAddress();

  /**
   * The remove SocketAddress where this Channel is bounded.
   * @return .
   */
  SocketAddress remoteAddress();

  /**
   * The final Pipeline this Channel has. Not shared.
   * Every channelEvent will be fired to the pipeline.
   * Every external will be coming from the pipeline.
   * @return .
   */
  ChannelPipeline getPipeline();

  /**
   * The final Transport this Channel has. Not shared.
   * Handles low-level I/O.
   * @return .
   */
  ChannelTransport getTransport();

  /**
   * The final TaskLoop this Channel has. Not shared.
   * It's used so the events are run in order.
   * @return .
   */
  TaskLoop getTaskLoop();

  /**
   * The Nexus this Channel is bounded to. Shared between all Channels.
   * @return .
   */
  Reactor getReactor();

  /**
   * Used to fire {@link java.nio.channels.Channel} reads.
   * @return for chaining. (Overiding type)
   */
  @Override
  Channel read();

  /**
   * Used to fire {@link java.nio.channels.Channel} writes.
   * @return for chaining. (Overriding type)
   */
  @Override
  Channel flush();
}

