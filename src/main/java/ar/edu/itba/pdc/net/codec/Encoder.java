package ar.edu.itba.pdc.net.codec;

import ar.edu.itba.pdc.net.async.Promise;
import ar.edu.itba.pdc.net.channel.HandlerContext;
import ar.edu.itba.pdc.net.channel.handler.OutputHandler;

public abstract class Encoder<I,O> implements OutputHandler {

  @Override
  @SuppressWarnings("unchecked")
  public void write(HandlerContext ctx, Object msg, Promise promise) throws Exception {
    if (this.shouldEncode(msg)) {
      ctx.write(this.encode((I)msg), promise);
    } else {
      ctx.write(msg, promise);
    }
  }

  /**
   * Just check if the msg is of type I.
   * @param msg message to check.
   * @return TRUE if the msg is of type I.
   */
  protected abstract boolean shouldEncode(Object msg);

  /**
   * Encode I to O.
   * @param msg the I object to encode.
   * @return an O object encoded.
   */
  protected abstract O encode(I msg) throws Exception;
}
