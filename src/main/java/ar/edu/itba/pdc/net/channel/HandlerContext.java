package ar.edu.itba.pdc.net.channel;

import ar.edu.itba.pdc.net.InputInvoker;
import ar.edu.itba.pdc.net.OutputInvoker;
import ar.edu.itba.pdc.net.channel.handler.Handler;

public interface HandlerContext extends InputInvoker, OutputInvoker {

  String getName();

  Channel getChannel();

  Handler getHandler();

  ChannelPipeline getPipeline();

  void setIsAdded(boolean isAdded);

  @Override
  HandlerContext fireChannelRegistered();

  @Override
  HandlerContext fireChannelUnregistered();

  @Override
  HandlerContext fireExceptionCaught(Exception cause);

  @Override
  HandlerContext fireChannelRead(Object msg);

  @Override
  HandlerContext fireChannelReadComplete();

  @Override
  HandlerContext read();

  @Override
  HandlerContext flush();
}
