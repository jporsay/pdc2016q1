package ar.edu.itba.pdc.net.channel;

import ar.edu.itba.pdc.net.InputInvoker;
import ar.edu.itba.pdc.net.OutputInvoker;
import ar.edu.itba.pdc.net.channel.handler.Handler;

public interface ChannelPipeline extends InputInvoker, OutputInvoker {

  ChannelPipeline addFirst(String name, Handler handler);

  ChannelPipeline addLast(String name, Handler handler);

  ChannelPipeline addBefore(String baseName, String name, Handler handler);

  ChannelPipeline addAfter(Handler baseName, String name, Handler handler);

  ChannelPipeline addAfter(String baseName, String name, Handler handler);

  Handler remove(String name);

  Handler remove(Handler handler);

  Handler replace(String oldName, String newName, Handler newHandler);

  Handler getFirst();

  HandlerContext getFirstContext();

  Handler getLast();

  HandlerContext getLastContext();

  Handler get(String name);

  HandlerContext getContext(String name);

  HandlerContext getContext(Handler handler);

  Channel getChannel();

  boolean hasContext(String name);
}
