package ar.edu.itba.pdc.net.async;

import ar.edu.itba.pdc.net.Nexus;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.util.concurrent.CompletableFuture;

public class TaskLoop extends Future {

  @SuppressFBWarnings(
      value = "NP_NONNULL_PARAM_VIOLATION",
      justification = "CompletableFuture methods always return CompletableFuture<Void>"
  )
  public TaskLoop(Nexus nexus) {
    super(nexus, CompletableFuture.completedFuture(null));
  }

}
