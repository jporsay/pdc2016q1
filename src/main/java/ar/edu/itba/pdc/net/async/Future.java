package ar.edu.itba.pdc.net.async;

import ar.edu.itba.pdc.net.Nexus;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

public class Future {
  private CompletableFuture<Void> future;
  private CompletableFuture<Void> lastFuture;
  private Nexus nexus;
  private static final Logger logger = LoggerFactory.getLogger(Future.class);
  private static final ExecutorService executor = Executors.newFixedThreadPool(
      Runtime.getRuntime().availableProcessors()
  );
  private CompletableFuture<Void> exceptionFuture = null;

  public Future(Nexus nexus) {
    this(nexus, new CompletableFuture<>());
  }

  protected Future(Nexus nexus, CompletableFuture<Void> javaFuture) {
    this.future = this.lastFuture = javaFuture;
    this.nexus = nexus;
  }

  /**
   * To complete this future.
   * @return me
   */
  @SuppressFBWarnings(
      value = "NP_NONNULL_PARAM_VIOLATION",
      justification = "CompletableFuture methods always return CompletableFuture<Void>"
  )
  public boolean complete() {
    this.future.exceptionally((cause) -> {
      logger.error("futuro roto", cause);
      return null;
    });
    return this.future.complete(null);
  }

  /**
   * To complete this future with an error.
   * @param cause the error.
   * @return me
   */
  public boolean completeExceptionally(Throwable cause) {
    return this.future.completeExceptionally(cause);
  }

  /**
   * To run something after the future.
   * @param action what to do.
   * @return me
   */
  public Future thenRun(Runnable action) {
    if (false && this.nexus.runningInReactor(Thread.currentThread())) {
      this.lastFuture = this.lastFuture.thenRunAsync(action, executor);
    } else {
      this.lastFuture = this.lastFuture.thenRun(action);
    }
    return this;
  }

  /**
   * To run something after the future failure.
   * @param fn what to do.
   * @return me
   */
  public Future exceptionally(Consumer<Throwable> fn) {
    if (exceptionFuture == null) {
      this.exceptionFuture = this.lastFuture.exceptionally(throwable -> {
        fn.accept(throwable.getCause());
        return null;
      });
    } else {
      logger.error("No e pueden correr dos handlers exceptionally");
    }
    return this;
  }

  public boolean isDone() {
    return this.future.isDone();
  }

  public boolean isCompletedExceptionally() {
    return this.future.isCompletedExceptionally();
  }

  public Void get(long timeout, TimeUnit unit) {
    try {
      return this.future.get(timeout, unit);
    } catch (InterruptedException | TimeoutException | ExecutionException cause) {
      cause.printStackTrace();
    }
    return null;
  }
}
