package ar.edu.itba.pdc.net;

public interface InputInvoker {

  InputInvoker fireChannelRegistered();

  InputInvoker fireChannelUnregistered();

  InputInvoker fireExceptionCaught(Exception cause);

  InputInvoker fireChannelRead(Object msg);

  InputInvoker fireChannelReadComplete();

}

