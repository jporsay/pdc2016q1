package ar.edu.itba.pdc.proxy;

import ar.edu.itba.pdc.admin.Stats;
import ar.edu.itba.pdc.config.Config;
import ar.edu.itba.pdc.mime.MimeAutomaton;
import ar.edu.itba.pdc.mime.MimeHandler;
import ar.edu.itba.pdc.net.ChannelInitializer;
import ar.edu.itba.pdc.net.async.Promise;
import ar.edu.itba.pdc.net.channel.Channel;
import ar.edu.itba.pdc.net.channel.ChannelPipeline;
import ar.edu.itba.pdc.net.channel.HandlerContext;
import ar.edu.itba.pdc.net.channel.handler.InputHandler;
import ar.edu.itba.pdc.net.channel.handler.OutputHandler;
import ar.edu.itba.pdc.net.codec.ByteBufferToLineEncoder;
import ar.edu.itba.pdc.net.codec.LineToByteBufferEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

/**
 * TODO: fixear el caso cuando una persona se equivoca de USER y tiene que DESCONECTAR DE SERVER1
 *       y conectar al servidor 2.
 * TODO: handle closed connections. (POP3 CLOSED or MAYBE WE CLOSED == GRACEFULLY CLOSE pop3)
 */
public class PopProxyHandler implements InputHandler {
  private static final Logger logger = LoggerFactory.getLogger(PopProxyHandler.class);
  private Channel serverChannel = null;
  private boolean first = true;
  private Promise serverChannelPromise = null;

  @Override
  public void channelRead(HandlerContext clientContext, Object clientMsg) throws Exception {
    String line = (String) clientMsg;
    if (this.serverChannel == null) {
      if (this.serverChannelPromise == null) {
        String[] commandAndUser = line.split(" ");
        InetSocketAddress endpoint = Config.get().getUserServerException(commandAndUser[1]);
        this.serverChannelPromise = clientContext.getChannel().createPromise();
        this.connectProxy(endpoint, clientContext, line);
      } else {
        logger.trace("QUEUED MSG {}", line);
        this.serverChannelPromise.thenRun(() -> {
          logger.trace("QUEUED MSG WRITE {}", line);
          this.serverChannel.writeAndFlush(line);
        });
      }
      return;
    }
    if (Config.get().shouldProcessResponses()) {
      this.processLine(line.toLowerCase(), clientContext);
    }
    this.serverChannel.writeAndFlush(line);
  }

  private void connectProxy(InetSocketAddress endpoint, HandlerContext clientContext, String line)
      throws IOException {
    logger.debug("Start PROXY connect to: {}", endpoint);
    PopProxyHandler me = this;
    clientContext.getChannel().getReactor().connect(endpoint,
        new ChannelInitializer("PROXY_SERVER") {
          @Override
          protected void initChannel(Channel channel) throws Exception {
            channel.getPipeline().addLast("PROXY_LINE_ENCODER", new LineToByteBufferEncoder());
            channel.getPipeline().addLast("PROXY_IN", new InputHandler() {
              @Override
              public void channelRegistered(HandlerContext ctx) throws Exception {
                logger.info("Proxy Server connected to: {}", endpoint);
                me.serverChannel = ctx.getChannel();
                ctx.writeAndFlush(line).thenRun(ctx.getChannel()::read).thenRun(() -> {
                  me.serverChannelPromise.complete();
                });
              }

              @Override
              public void channelRead(HandlerContext ctx, Object msg) throws Exception {
                if (me.first) {
                  ((ByteBuffer) msg).clear();
                  me.first = false;
                  return;
                }
                clientContext.writeAndFlush(msg);
              }

            });
            channel.getPipeline().addFirst("PROXY_OUT", new OutputHandler() {

              @Override
              public void close(HandlerContext ctx, Promise promise) throws Exception {
                clientContext.close();
                logger.info("CLOSE_CLOSE_CLOSE_CLOSE");
              }
            });
          }
        }
    );
  }

  private void processLine(String line, HandlerContext clientContext) {
    if (line.startsWith("top ") || line.startsWith("retr ")) {
      ChannelPipeline pipeline = clientContext.getPipeline();
      if (!pipeline.hasContext("mimeHandler")) {
        Stats.emailsProcessed.addAndGet(1);
        pipeline.addBefore("PROXY", "byteBufferToLine", new ByteBufferToLineEncoder());
        pipeline.addBefore("byteBufferToLine", "mimeHandler", new MimeHandler());
      }
      if (!pipeline.hasContext("stateMachineParser")) {
        pipeline.addBefore("mimeHandler", "stateMachineParser", new MimeAutomaton());
      }
    }
  }
}
