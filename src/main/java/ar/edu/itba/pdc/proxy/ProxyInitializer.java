package ar.edu.itba.pdc.proxy;

import ar.edu.itba.pdc.net.ChannelInitializer;
import ar.edu.itba.pdc.net.channel.Channel;
import ar.edu.itba.pdc.net.codec.ByteBufferToLineDecoder;
import ar.edu.itba.pdc.net.codec.LineToByteBufferEncoder;
import ar.edu.itba.pdc.stats.ByteStatsHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProxyInitializer extends ChannelInitializer {
  private static final Logger logger = LoggerFactory.getLogger(ProxyInitializer.class);

  public ProxyInitializer() {
    super("PROXY");
  }

  @Override
  protected void initChannel(Channel channel) throws Exception {
    channel.getPipeline().addLast("BYTE_STATS", new ByteStatsHandler());
    channel.getPipeline().addLast("decodeLine", new ByteBufferToLineDecoder());
    channel.getPipeline().addLast("encodeLine", new LineToByteBufferEncoder());
    channel.getPipeline().addLast("userBouncer", new UserBouncerHandler());
    channel.writeAndFlush("+OK Welcome to the Nexus");
    channel.read();
  }
}
