package ar.edu.itba.pdc.proxy;

import ar.edu.itba.pdc.net.channel.HandlerContext;
import ar.edu.itba.pdc.net.channel.handler.InputHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserBouncerHandler implements InputHandler {
  private static final Logger logger = LoggerFactory.getLogger(UserBouncerHandler.class);

  @Override
  public void channelRead(HandlerContext ctx, Object msg) throws Exception {
    String line = (String) msg;
    logger.info("Bouncer {}", line);
    if (!line.toLowerCase().contains("user ")) {
      ctx.writeAndFlush("-ERR Waiting for USER command");
      return;
    }
    ctx.getPipeline().addAfter(this, "PROXY", new PopProxyHandler());
    ctx.getPipeline().remove(this);
    ctx.fireChannelRead(msg);
  }
}
