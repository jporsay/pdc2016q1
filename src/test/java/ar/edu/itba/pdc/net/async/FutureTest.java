package ar.edu.itba.pdc.net.async;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import ar.edu.itba.pdc.net.Nexus;

import org.junit.Test;

public class FutureTest {

  @Test
  public void complete() throws Exception {
    Nexus nexus = mock(Nexus.class);
    // Syncronous for test.
    when(nexus.runningInReactor(any())).thenReturn(false);
    Future future = new Future(nexus);
    // initial status
    assertEquals(false, future.isDone());
    assertEquals(false, future.isCompletedExceptionally());
    StringBuilder sb = new StringBuilder();

    // define future.
    future.thenRun(
        () -> sb.append("Uno")
    ).thenRun(
        () -> sb.append("Dos")
    ).exceptionally(
        (cause) -> fail(cause.getMessage())
    );

    // assert instant.
    assertEquals(false, future.isDone());
    assertEquals(false, future.isCompletedExceptionally());
    assertEquals("", sb.toString());

    // assert future
    future.complete();
    assertEquals(true, future.isDone());
    assertEquals(false, future.isCompletedExceptionally());
    assertEquals("UnoDos", sb.toString());
  }

  @Test
  public void completeExceptionally() throws Exception {
    Nexus nexus = mock(Nexus.class);
    // Syncronous for test.
    when(nexus.runningInReactor(any())).thenReturn(false);
    Future future = new Future(nexus);
    // initial status
    assertEquals(false, future.isDone());
    assertEquals(false, future.isCompletedExceptionally());
    StringBuilder sb = new StringBuilder();

    // define future.
    future.thenRun(
        () -> fail("UNEXPECTED RUN")
    ).exceptionally(
        (cause) -> sb.append(":1:").append(cause.getMessage())
    );

    // assert instant.
    assertEquals(false, future.isDone());
    assertEquals(false, future.isCompletedExceptionally());
    assertEquals("", sb.toString());

    // assert future
    future.completeExceptionally(new Exception("ERR"));
    assertEquals(true, future.isDone());
    assertEquals(true, future.isCompletedExceptionally());
    assertEquals(":1:ERR", sb.toString());
  }

}