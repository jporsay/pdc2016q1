package ar.edu.itba.pdc.net.codec;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import ar.edu.itba.pdc.net.channel.HandlerContext;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;

public class ByteBufferToLineDecoderTest {


  private ByteBufferToLineDecoder decoder;

  @Before
  public void setUp() {
    this.decoder = new ByteBufferToLineDecoder();
  }

  @Test
  public void channelRead() throws Exception {
    HandlerContext ctx = mock(HandlerContext.class);
    ByteBuffer buffer = ByteBuffer.wrap(
        "12\r\n\r\n345\r\n6\r\n789".getBytes(Charset.forName("US-ASCII"))
    ).compact();
    ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
    this.decoder.channelRead(ctx, buffer);
    verify(ctx, times(4)).fireChannelRead(captor.capture());
    List<String> lines = captor.getAllValues();
    assertEquals("12", lines.get(0));
    assertEquals("", lines.get(1));
    assertEquals("345", lines.get(2));
    assertEquals("6", lines.get(3));
    captor = ArgumentCaptor.forClass(String.class);
    buffer.put("0\r\n".getBytes(Charset.forName("US-ASCII")));
    this.decoder.channelRead(ctx, buffer);
    verify(ctx, times(5)).fireChannelRead(captor.capture());
    lines = captor.getAllValues();
    assertEquals("7890", lines.get(4));
  }

}
