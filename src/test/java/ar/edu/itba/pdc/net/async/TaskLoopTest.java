package ar.edu.itba.pdc.net.async;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import ar.edu.itba.pdc.net.Nexus;

import org.junit.Test;

public class TaskLoopTest {

  @Test
  public void newInstance() {
    Nexus nexus = mock(Nexus.class);
    // Syncronous for test.
    when(nexus.runningInReactor(any())).thenReturn(false);
    TaskLoop taskLoop = new TaskLoop(nexus);
    assertTrue(taskLoop.isDone());
  }

}