package ar.edu.itba.pdc.test.command;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import ar.edu.itba.pdc.admin.CommandFactory;
import ar.edu.itba.pdc.admin.command.Command;
import ar.edu.itba.pdc.admin.command.DefaultServerSetCommand;
import ar.edu.itba.pdc.admin.command.ErrorCommand;

import org.junit.Test;

public class CommandFactoryTest {

  @Test
  public void commandIsNotAcceptedWithoutSpaceAfterName() {
    Command command = (new CommandFactory())
        .fromLine(DefaultServerSetCommand.COMMAND + "google.com");
    assertTrue(command instanceof ErrorCommand);
    assertNull(command.getUsage());
    assertNull(command.getDescription());
    assertTrue(command.execute().isError());
  }

  @Test
  public void invalidCommands() {
    Command command = (new CommandFactory()).fromLine("HOL");
    assertTrue(command instanceof ErrorCommand);
    assertTrue(command.execute().isError());
    command = (new CommandFactory()).fromLine("HOLA");
    assertTrue(command instanceof ErrorCommand);
    assertTrue(command.execute().isError());
    command = (new CommandFactory()).fromLine("HOLAAAA");
    assertTrue(command instanceof ErrorCommand);
    assertTrue(command.execute().isError());
  }

}
