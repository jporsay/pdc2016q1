package ar.edu.itba.pdc.test.command;

import static org.junit.Assert.assertTrue;

import ar.edu.itba.pdc.admin.CommandFactory;
import ar.edu.itba.pdc.admin.command.Command;
import ar.edu.itba.pdc.admin.command.StatsCommand;
import ar.edu.itba.pdc.admin.response.CommandResponse;
import ar.edu.itba.pdc.admin.response.OkResponse;

import org.junit.Test;

public class StatCommandTest {

  //FIXME: We still need to test everything else.

  @Test
  public void statCommandReturns() {
    Command command = (new CommandFactory()).fromLine(StatsCommand.COMMAND);
    CommandResponse response = command.execute();
    assertTrue(response instanceof OkResponse);
    String message = response.toString();
    assertTrue(message.contains("images_rotated"));
    assertTrue(message.contains("leetified_subjects"));
    assertTrue(message.contains("emails_processed"));
    assertTrue(message.contains("accepted_connections"));
  }
}
