package ar.edu.itba.pdc.test.command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import ar.edu.itba.pdc.admin.CommandFactory;
import ar.edu.itba.pdc.admin.command.Command;
import ar.edu.itba.pdc.admin.command.DefaultServerSetCommand;
import ar.edu.itba.pdc.admin.response.CommandResponse;
import ar.edu.itba.pdc.admin.response.MisuseErrorResponse;
import ar.edu.itba.pdc.config.Config;
import ar.edu.itba.pdc.config.InMemoryConfigDao;

import org.junit.BeforeClass;
import org.junit.Test;

import java.net.InetSocketAddress;

public class DefaultServerSetCommandTest {

  private static InetSocketAddress DEFAULT_SERVER = new InetSocketAddress("default.com", 110);

  @BeforeClass
  public static void onceSetup() {
    Config.get().setDao(new InMemoryConfigDao());
    Config.get().load();
    Config.get().setDefaultServer(DEFAULT_SERVER);
  }

  @Test
  public void payloadIsSetCorrectlyToDefaultServer() {
    String expectedServer = "localhost:110";
    Command command = (new CommandFactory()).fromLine(
        DefaultServerSetCommand.COMMAND + "      " + expectedServer + "    "
    );
    assertTrue(command instanceof DefaultServerSetCommand);
    DefaultServerSetCommand setCommand = (DefaultServerSetCommand) command;
    assertFalse(setCommand.execute().isError());
    assertEquals(Config.get().getDefaultServerEndpoint(), new InetSocketAddress("localhost", 110));
  }

  @Test
  public void responseIsErrorWithEmptyPayload() {
    Command command = (new CommandFactory()).fromLine(DefaultServerSetCommand.COMMAND);
    CommandResponse response = command.execute();
    assertTrue(response.isError());
    command = (new CommandFactory()).fromLine(DefaultServerSetCommand.COMMAND + "    ");
    response = command.execute();
    assertTrue(response.isError());
    assertTrue(response instanceof MisuseErrorResponse);
    assertTrue(response.getMessage().contains("ERR"));
    assertTrue(response.getMessage().contains(command.getUsage()));
  }

  @Test
  public void commandIsValidWithWeirdCapitalization() {
    String commandName = DefaultServerSetCommand.COMMAND;
    commandName = commandName.substring(0, 1).toLowerCase() + commandName.substring(1);
    commandName = commandName.substring(0, 3) + commandName.substring(3).toLowerCase();
    Command command = (new CommandFactory()).fromLine(commandName + " google.com");
    assertTrue(command instanceof DefaultServerSetCommand);

  }

  @Test
  public void setToValidServer() {
    assertTrue(this.defaultMatches("localhost:110"));
    assertTrue(this.defaultMatches("127.0.0.1:110"));
  }

  private boolean defaultMatches(String serverString) {
    String[] serverPort = serverString.split(":");
    DefaultServerSetCommand command = new DefaultServerSetCommand(serverString);
    CommandResponse response = command.execute();
    assertFalse(response == null);
    assertFalse(response.isError());
    return Config.get().getDefaultServerEndpoint().equals(
        new InetSocketAddress(serverPort[0], Integer.parseInt(serverPort[1]))
    );
  }

  @Test
  public void setToInvalidServer() {
    String serverString = "thishostshouldnotexist.com";
    DefaultServerSetCommand command = new DefaultServerSetCommand(serverString);
    CommandResponse response = command.execute();
    assertFalse(response == null);
    assertTrue(response.isError());
    assertEquals(Config.get().getDefaultServerEndpoint(), DEFAULT_SERVER);
  }
}
