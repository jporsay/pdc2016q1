package ar.edu.itba.pdc.test.command;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

import ar.edu.itba.pdc.admin.CommandFactory;
import ar.edu.itba.pdc.admin.command.Command;
import ar.edu.itba.pdc.admin.command.UserServerSetCommand;
import ar.edu.itba.pdc.config.Config;
import ar.edu.itba.pdc.config.InMemoryConfigDao;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.InetSocketAddress;

public class UserServerSetCommandTest {

  @BeforeClass
  public static void onceSetup() {
    Config.get().setDao(new InMemoryConfigDao());
    Config.get().load();
    Config.get().setDefaultServer(new InetSocketAddress("default.com", 110));
  }

  @Before
  public void setUp() {
    Config.get().getUserServerExceptions().clear();
  }

  @Test
  public void errorResponseWithNoParameters() {
    Command command = (new CommandFactory()).fromLine(UserServerSetCommand.COMMAND);
    assertTrue(command.execute().isError());
  }

  @Test
  public void errorResponseWithOneParameter() {
    String user = "jporsay";
    Command command = this.getCommand(" " + user + "    ");
    assertTrue(command.execute().isError());
    assertEquals(
        Config.get().getUserServerException(user), Config.get().getDefaultServerEndpoint()
    );

  }

  @Test
  public void errorResponseOnInvertedParameters() {
    String user = "jporsay";
    Command command = this.getCommand(" google.com " + user);
    assertTrue(command.execute().isError());
    assertEquals(
        Config.get().getUserServerException(user), Config.get().getDefaultServerEndpoint()
    );
  }

  @Test
  public void correctResponseOnCorrectParameters() {
    String user = "jporsay";
    String server = "google.com:111";
    Command command = this.getCommand(" " + user + " " + server + " ");
    assertFalse(command.execute().isError());
    assertEquals(
        Config.get().getUserServerException(user), new InetSocketAddress("google.com", 111)
    );
  }

  private Command getCommand(String parameters) {
    return (new CommandFactory()).fromLine(UserServerSetCommand.COMMAND + parameters);
  }
}
