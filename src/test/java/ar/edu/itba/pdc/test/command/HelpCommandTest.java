package ar.edu.itba.pdc.test.command;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

import ar.edu.itba.pdc.admin.CommandFactory;
import ar.edu.itba.pdc.admin.command.Command;
import ar.edu.itba.pdc.admin.command.HelpCommand;
import ar.edu.itba.pdc.admin.response.CommandResponse;
import ar.edu.itba.pdc.config.Config;
import ar.edu.itba.pdc.config.InMemoryConfigDao;

import org.junit.BeforeClass;
import org.junit.Test;

public class HelpCommandTest {

  @BeforeClass
  public static void onceSetup() {
    Config.get().setDao(new InMemoryConfigDao());
    Config.get().load();
  }

  @Test
  public void helpAppreciated() {
    CommandResponse response = this.getCommand().execute();
    assertFalse(response.isError());
    String message = response.getMessage();
    assertTrue(message.contains("Show this help"));
    CommandFactory.COMMANDS.forEach((key, value) -> {
      assertTrue(message.contains(key));
    });
  }

  private Command getCommand() {
    return (new CommandFactory()).fromLine(HelpCommand.COMMAND);
  }
}
