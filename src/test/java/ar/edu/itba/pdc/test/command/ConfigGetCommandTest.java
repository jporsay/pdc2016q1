package ar.edu.itba.pdc.test.command;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

import ar.edu.itba.pdc.admin.CommandFactory;
import ar.edu.itba.pdc.admin.command.Command;
import ar.edu.itba.pdc.admin.command.ConfigGetCommand;
import ar.edu.itba.pdc.admin.response.CommandResponse;
import ar.edu.itba.pdc.config.Config;
import ar.edu.itba.pdc.config.InMemoryConfigDao;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.InetSocketAddress;

public class ConfigGetCommandTest {

  private static final InetSocketAddress DEFAULT_SERVER = new InetSocketAddress("google.com", 110);

  @BeforeClass
  public static void onceSetup() {
    Config.get().setDao(new InMemoryConfigDao());
    Config.get().load();
  }

  @Before
  public void setUp() {
    Config.get().setLeetEnabled(true);
    Config.get().setImageFlipEnabled(true);
    Config.get().clearUserServerExceptions();
    Config.get().setDefaultServer(DEFAULT_SERVER);
  }

  @Test
  public void allSettingsPresent() {
    Command command = (new CommandFactory()).fromLine(ConfigGetCommand.COMMAND);
    CommandResponse response = command.execute();
    assertFalse(response.isError());
    String message = response.getMessage();
    assertTrue(
        message.contains("CONF defaultServer " + this.encodeInetSocketAddress(DEFAULT_SERVER))
    );
    assertTrue(message.contains("CONF leetMode true"));
    assertTrue(message.contains("CONF imageFlip true"));
    assertTrue(message.contains("CONF serverExceptions none"));
  }

  @Test
  public void leetModeUpdated() {
    Command command = (new CommandFactory()).fromLine(ConfigGetCommand.COMMAND);
    Config.get().setLeetEnabled(false);
    CommandResponse response = command.execute();
    assertFalse(response.isError());
    assertTrue(response.getMessage().contains("CONF leetMode false"));
  }

  @Test
  public void imageFlipUpdated() {
    Command command = (new CommandFactory()).fromLine(ConfigGetCommand.COMMAND);
    Config.get().setImageFlipEnabled(false);
    CommandResponse response = command.execute();
    assertFalse(response.isError());
    assertTrue(response.getMessage().contains("CONF imageFlip false"));
  }

  @Test
  public void defaultServerUpdated() {
    Command command = (new CommandFactory()).fromLine(ConfigGetCommand.COMMAND);
    Config.get().setDefaultServer(new InetSocketAddress("microsoft.com", 110));
    CommandResponse response = command.execute();
    assertFalse(response.isError());
    assertTrue(response.getMessage().contains("CONF defaultServer microsoft.com:110"));
  }

  @Test
  public void userServerExceptionsUpdated() {
    Command command = (new CommandFactory()).fromLine(ConfigGetCommand.COMMAND);
    Config.get().setUserServerException("jporsay", new InetSocketAddress("microsoft.com", 110));
    Config.get().setUserServerException("qcho", new InetSocketAddress("datasoft.com", 110));
    CommandResponse response = command.execute();
    assertFalse(response.isError());
    String message = response.getMessage();
    assertTrue(
        message.contains("CONF serverExceptions jporsay@microsoft.com:110,qcho@datasoft.com:110")
    );
  }

  private String encodeInetSocketAddress(InetSocketAddress address) {
    return address.getHostName() + ":" + address.getPort();
  }
}
