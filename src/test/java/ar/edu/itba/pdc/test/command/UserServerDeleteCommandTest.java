package ar.edu.itba.pdc.test.command;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

import ar.edu.itba.pdc.admin.CommandFactory;
import ar.edu.itba.pdc.admin.command.Command;
import ar.edu.itba.pdc.admin.command.UserServerDeleteCommand;
import ar.edu.itba.pdc.admin.response.CommandResponse;
import ar.edu.itba.pdc.admin.response.MisuseErrorResponse;
import ar.edu.itba.pdc.config.Config;
import ar.edu.itba.pdc.config.InMemoryConfigDao;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.InetSocketAddress;

public class UserServerDeleteCommandTest {

  private static InetSocketAddress DEFAULT_SERVER = new InetSocketAddress("default.com", 110);

  @BeforeClass
  public static void onceSetup() {
    Config.get().setDao(new InMemoryConfigDao());
    Config.get().load();
    Config.get().setDefaultServer(DEFAULT_SERVER);
  }

  @Before
  public void setUp() {
    Config.get().clearUserServerExceptions();
  }

  @Test
  public void errorResponseWithNoParameters() {
    Command command = (new CommandFactory()).fromLine(UserServerDeleteCommand.COMMAND);
    CommandResponse response = command.execute();
    assertTrue(response.isError());
    assertTrue(response instanceof MisuseErrorResponse);
  }

  @Test
  public void okResponseEvenWithoutException() {
    String user = "jporsay";
    Command command = this.getCommand(user + "   ");
    assertFalse(command.execute().isError());
    assertEquals(
        Config.get().getUserServerException(user), Config.get().getDefaultServerEndpoint()
    );

  }

  @Test
  public void correctResponseOnCorrectParameters() {
    String user = "jporsay";
    InetSocketAddress server = new InetSocketAddress("google.com", 110);
    Config.get().setUserServerException(user, server);
    assertEquals(Config.get().getUserServerException(user), server);
    Command command = this.getCommand(user);
    command.execute();
    assertEquals(Config.get().getUserServerException(user), DEFAULT_SERVER);
  }

  @Test
  public void misuseOnMultipleParameters() {
    String user = "jporsay ";
    Command command = this.getCommand(user + user);
    CommandResponse response = command.execute();
    assertTrue(response.isError());
    assertTrue(response.getMessage().contains("ERR"));
    assertTrue(response.getMessage().contains(command.getUsage()));
  }

  private Command getCommand(String parameters) {
    return (new CommandFactory()).fromLine(UserServerDeleteCommand.COMMAND + " " + parameters);
  }
}
