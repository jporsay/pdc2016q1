package ar.edu.itba.pdc.test.command;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

import ar.edu.itba.pdc.admin.CommandFactory;
import ar.edu.itba.pdc.admin.command.Command;
import ar.edu.itba.pdc.admin.command.LeetModeSetCommand;
import ar.edu.itba.pdc.config.Config;
import ar.edu.itba.pdc.config.InMemoryConfigDao;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class LeetCommandTest {

  @BeforeClass
  public static void onceSetup() {
    Config.get().setDao(new InMemoryConfigDao());
    Config.get().load();
  }

  @Before
  public void setUp() {
    Config.get().setLeetEnabled(true);
  }

  @Test
  public void errorResponseWithNoParameters() {
    Command command = (new CommandFactory()).fromLine(LeetModeSetCommand.COMMAND);
    assertTrue(command.execute().isError());
  }

  @Test
  public void errorResponseOnInvalidParameters() {
    String[] parameters = {"derp", "1 0", "00", "0e", "1e"};
    for (String parameter : parameters) {
      assertTrue(this.getCommand(parameter).execute().isError());
      assertTrue(Config.get().isLeetEnabled());
    }
  }

  @Test
  public void correctResponseOnCorrectParameters() {
    assertFalse(this.getCommand("1").execute().isError());
    assertTrue(Config.get().isLeetEnabled());
    assertFalse(this.getCommand("0").execute().isError());
    assertFalse(Config.get().isLeetEnabled());
  }

  private Command getCommand(String parameters) {
    return (new CommandFactory()).fromLine(LeetModeSetCommand.COMMAND + " " + parameters);
  }
}
