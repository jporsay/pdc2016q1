package ar.edu.itba.pdc.test.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;

import ar.edu.itba.pdc.config.Config;
import ar.edu.itba.pdc.config.ConfigDao;
import ar.edu.itba.pdc.config.InMemoryConfigDao;
import ar.edu.itba.pdc.config.PropertyConfig;
import ar.edu.itba.pdc.config.PropertyConfigDao;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertyConfigDaoTest {

  @BeforeClass
  public static void onceSetup() {
    Config.get().setDao(new InMemoryConfigDao());
    Config.get().load();
  }

  @Test
  public void testFileConfigDao() {
    Path propertyFilePath = Config.get().getPropertyConfigPath();
    PropertyConfigDao dao = new PropertyConfigDao(propertyFilePath);
    Config.get().setDao(dao);
    InetSocketAddress defaultServer = new InetSocketAddress("google.com", 110);
    Config config = Config.get();
    config.setDefaultServer(defaultServer);
    Map<String, InetSocketAddress> userServer = new HashMap<>();
    userServer.put("john", new InetSocketAddress("yahoo.com", 110));
    userServer.put("paul", new InetSocketAddress("reddit.com", 110));
    userServer.put("roberto", new InetSocketAddress("sarasa.com", 110));
    Config.get().setUserServerExceptionMap(userServer);
    config.load();
    assertEquals(config.getDefaultServerEndpoint(), defaultServer);
    userServer.forEach((key, value) -> {
      assumeTrue(Config.get().getUserServerException(key).equals(value));
    });
  }

  @Test
  public void testIoExceptionWhenLoading() throws IOException {
    Properties prop = mock(Properties.class);
    Mockito.doThrow(new IOException()).when(prop).load(Matchers.<InputStream>any());
    ConfigDao dao = new PropertyConfigDao(Config.get().getPropertyConfigPath());
    Config.get().setDao(dao);
    dao.load();

  }

  @Test
  public void testIoExceptionWhenWriting() throws IOException {
    Properties prop = mock(Properties.class);
    Mockito.doThrow(new IOException()).when(prop).store(Matchers.<OutputStream>any(), anyString());
    PropertyConfigDao dao = new PropertyConfigDao(Config.get().getPropertyConfigPath());
    Config.get().setDao(dao);
    dao.write(Config.get());
  }

  @Test
  public void testInvalidUserServerExceptionStoredValue() throws IOException {
    Config config = Config.get();
    Path configPath = config.getPropertyConfigPath();
    config.setDao(new PropertyConfigDao(configPath));
    config.clearUserServerExceptions();
    PropertyConfig propConfig = new PropertyConfig();
    Properties configProperties = propConfig.toProperties(config);
    configProperties.remove("user_server_exception");
    configProperties.setProperty("user_server_exception", ",,jporsay,qcho@groso.com:110");
    (new PropertyConfig(configProperties)).populateConfig(config);
    assertEquals(1, config.getUserServerExceptions().size());
  }
}
