package ar.edu.itba.pdc.admin;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import ar.edu.itba.pdc.admin.command.ConfigGetCommand;
import ar.edu.itba.pdc.admin.command.DefaultServerSetCommand;
import ar.edu.itba.pdc.admin.command.HelpCommand;
import ar.edu.itba.pdc.admin.command.ImageFlipSetCommand;
import ar.edu.itba.pdc.admin.command.LeetModeSetCommand;
import ar.edu.itba.pdc.admin.command.StatsCommand;
import ar.edu.itba.pdc.admin.command.UserServerDeleteCommand;
import ar.edu.itba.pdc.admin.command.UserServerSetCommand;

import org.junit.Test;

public class CommandExecutorTest {
  @Test
  public void shouldEncode() throws Exception {
    CommandExecutor executor = new CommandExecutor();
    assertFalse(executor.shouldEncode("hey!"));
    assertFalse(executor.shouldEncode(3));
    assertTrue(executor.shouldEncode(new StatsCommand()));
    assertTrue(executor.shouldEncode(new ConfigGetCommand()));
    assertTrue(executor.shouldEncode(new UserServerSetCommand("")));
    assertTrue(executor.shouldEncode(new DefaultServerSetCommand("")));
    assertTrue(executor.shouldEncode(new UserServerDeleteCommand("")));
    assertTrue(executor.shouldEncode(new LeetModeSetCommand("")));
    assertTrue(executor.shouldEncode(new ImageFlipSetCommand("")));
    assertTrue(executor.shouldEncode(new HelpCommand()));
  }

  @Test
  public void encode() throws Exception {
    CommandExecutor executor = new CommandExecutor();
    String response = executor.encode(new StatsCommand());
    assertTrue(response.contains("+OK"));
    assertFalse(response.contains("\r\n"));
  }

}
